#include <stdio.h>
#include <stdlib.h>
#include <time.h>
#include <math.h>

double expoDist(double avrg)
{
    int numRan;
    int min = 0, max = 10, count = 0;

    srand(time(0));
    double waiting_t;

    numRan = (rand() % (max - min + 1)) + min; 

    waiting_t = avrg*exp(avrg*numRan);

    return waiting_t;
}