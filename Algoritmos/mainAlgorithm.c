#include <stdio.h>
struct parametros{
    int size;
    char bridge;
    int cant; 
    int timeN; 
    int timeS;
    int algorithm; 
    int BrigdeWeight; 
    int scheduler; 
};

int checkAvailability(char bridge,int map[32][50]);
//void * AlgorithmsThread(void *arg);

void mainAlgorithm(int size, char bridge, int cant, int timeN, int timeS, int algorithm, int BrigdeWeight, int scheduler, struct Alien aliensA[], struct Alien aliensM[],int *mode,int map[32][50], int aliensFlags[],int go){
    int row=13;
    int column=16;
    int CardinalP, CardinalP1;
    int disponible=checkAvailability(bridge,map);
    struct Alien listN[size];
    struct Alien listS[size];
    while(go){
        
        //inicializamos valores
        for(int i=0; i<size;i++){
            //Aliens Norte
            listN[i].moveSpeed=0;
            listN[i].alienID=0;
            listN[i].bridge_ID='1';
            listN[i].maxExeTime=0;
            listN[i].column=0;
            listN[i].row=0;
            listN[i].priority=0;
            listN[i].weight=0;
            //Aliens Sur
            listS[i].moveSpeed=0;
            listS[i].alienID=0;
            listS[i].bridge_ID='1';
            listS[i].maxExeTime=0;
            listS[i].column=0;
            listS[i].row=0;
            listS[i].priority=0;
            listS[i].weight=0;
        }
        
        if (bridge=='1'){
            row=13;
            column=16; 
        }
        else if(bridge=='2'){
            row=13;
            column=24; //modificar
        }
        else
        {
            row=13;
            column=32; //modificar
        }
        //printf("Row= %d Column= %d \n",row,column);
        CardinalP=1;
        CardinalP1=0;
        if(*mode == 1){
            Select(size, column, row, CardinalP, aliensA, listN,map);
            Select(size, column, row, CardinalP1, aliensA, listS,map);
        }
        else if(*mode == 2){
            Select(size, column, row, CardinalP, aliensM, listN,map);
            Select(size, column, row, CardinalP1, aliensM, listS,map);

        }
        
        
        
        /* for(int i=0; i<size;i++){
            printf("ORIGINAL %d\n",list[i].alienID);
        } */
        
        if(scheduler==2){
            SJF_scheduling(size, listN);
            SJF_scheduling(size, listS);
        }
        else if(scheduler==3){
            FIFO_scheduling(size, listN);
            FIFO_scheduling(size, listS);
        }
        else if(scheduler==4){
            lottery_scheduling(size, listN);
            lottery_scheduling(size, listS);
        }
        else{
            priority_scheduling(size, listN);
            priority_scheduling(size, listS);
        }
        /* Write(size, column, row, 1, aliens, listN,map);
        Write(size, column, row, 0, aliens, listS,map); */
        
        /* for(int i=0;i<size; i++){

            printf("Lista Norte %d\n",listN[i].alienID);
            printf("Lista Sur %d\n",listS[i].alienID);
        } */
        
        if(disponible){
            if(algorithm==1){
                Y_Algorithm(size, cant, BrigdeWeight, listN, listS, aliensFlags,map,bridge);
            } 
            else if(algorithm==2){
                Semaphore_Algorithm(size, timeN, timeS, BrigdeWeight, listN, listS, aliensFlags,map,bridge);
            }
        }
        
        /* for(int i=0; i<size;i++){
            printf("Priority Scheduling %d\n",list[i].alienID);
        } */
    }
    pthread_exit(NULL);
    
}

/* void * AlgorithmsThread(void *arg){
    struct parametros * data1;
    struct parametros data;
    data1=(struct parametros *)arg;
    data=*data1;
    int size=data.size;
    char bridge=data.bridge;
    int cant=data.cant; 
    int timeN=data.timeN; 
    int timeS=data.timeS;
    int algorithm=data.algorithm; 
    int BrigdeWeight=data.BrigdeWeight; 
    int scheduler=data.scheduler; 

    mainAlgorithm(size, bridge, cant, timeN, timeS, algorithm, BrigdeWeight, scheduler, aliens,map,aliensFlags); //color nombre correcto a esas últimas
}
 */