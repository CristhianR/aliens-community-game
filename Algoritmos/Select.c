#include <stdio.h>


void Select(int size, int x, int y, int CardinalP, struct Alien aliens[], struct Alien list[size], int map[32][50]){ 
    for(int i=0; i<size; i++){
        int posx=x; //se toma pos x
        int posy;
        if(CardinalP){ //si es 1 es norte si es 0 es sur
            posy=y-i; //se toma pos y
        }
        else
        {
            posx=x+1;
            y=20;
            posy=y+i; //se toma pos y
        }

        int id=map[posy][posx]; //se toma el id del mapa

        if(id!=0){
            list[i]=aliens[id-1]; //meter alien en la lista poner -1
        }
        
    }
 return;
}

void Write(int size, int x, int y, int CardinalP, struct Alien aliens[], struct Alien list[size], int map[32][50]){ 
    for(int i=0; i<size; i++){
        int posx=x; //se toma pos x
        int posy;
        if(CardinalP){ //si es 1 es norte si es 0 es sur
            posy=y-i; //se toma pos y
        }
        else
        {
            y=20;
            posy=y+i; //se toma pos y
            posx=x+1;
        }
        //map[posy][posx]=0; //se toma el id del mapa
        list[i].column=posx; //actualizar posiciones
        list[i].row=posy; 
        list[i].x=20*posx;
        list[i].y=20*posy;
    }
 return;
}

int checkAvailability(char bridge,int map[32][50]){
    int row=13;
    int column=16;
    int disponible=1;

    if (bridge=='1'){
        row=13;
        column=16; 
    }
    else if(bridge=='2'){
        row=13;
        column=24; //modificar
    }
    else
    {
        row=13;
        column=32; //modificar
    }
    for(int i=0; i<6; i++){
        row++;
        if(map[row][column]!=0){
            disponible=0;
        }
        else if(map[14][17]!=0){
            disponible=0;
        }
        else if(map[14][25]!=0){
            disponible=0;
        }
        else if(map[14][33]!=0){
            disponible=0;
        }
        else if(map[19][17]!=0){
            disponible=0;
        }
        else if(map[19][25]!=0){
            disponible=0;
        }
        else if(map[19][33]!=0){
            disponible=0;
        }
    }
    return disponible;
}
