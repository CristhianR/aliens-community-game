#include <stdio.h>
#include <unistd.h>
#include <allegro5/allegro.h>
#include <allegro5/allegro_font.h>
#include <allegro5/allegro_ttf.h> // Cristhian Rojas: 26072020
#include <allegro5/drawing.h>
#include <allegro5/allegro_primitives.h>
#include <allegro5/allegro_image.h>
#include <time.h>
#include <pthread.h>
#include <stdlib.h>
#include <assert.h>
#include "exponential_distribution.c"
#include "poisson_distribution.c"
struct  Alien
{
	float x;
	float y;
	float moveSpeed;
	int weight;
	int community;
	int type;
	int count_done;
	ALLEGRO_BITMAP *alien;
	char bridge_ID;
	int bridge_pass;
	int row; 
	int column;
	float cont; 
	int alienID; 
	int mapPos; 
	int priority;
    int maxExeTime;
	pthread_t tAlien;
};
#include "Algoritmos/Calendarización/priority_scheduling.c"
#include "Algoritmos/Select.c" 
#include "Algoritmos/Calendarización/FIFO_scheduling.c"
#include "Algoritmos/Puente/Y_Algorithm.c"
#include "Algoritmos/Calendarización/lottery_scheduling.c"
#include "Algoritmos/Calendarización/SJF_scheduling.c"
#include "Algoritmos/Puente/Semaphore_Algorithm.c"
#include "Algoritmos/mainAlgorithm.c"


const float FPS = 60;
int size = 50;

#define screenW 1000
#define screenH 700
#define blockSize 20
#define num_aliens 150 //jrivera 20200724

int loadCounterX = 0, loadCounterY = 0, mapSizeX = 50, mapSizeY = 50; 
int map[100][100];
int posMat[33][50]; //jrivera 20200724
int aliensFlags[num_aliens]; //jrivera 20200724
int go = 1; //jrivera 20200724
int bridge1_algorithm;
int bridge2_algorithm;
int bridge3_algorithm;
int bridge1_candelarizador;
int bridge2_candelarizador;
int bridge3_candelarizador;
int bridge1_weight;
int bridge2_weight;
int bridge3_weight;
int count_aliens_manual = 0;
struct Alien *aliens[num_aliens]; // Aliens structs array
struct Alien *aliensM[num_aliens]; // Cristhian Rojas: 26072020

const char* ALIEN_NORMAL_CONFIG = "./configuration/alien/alienNormal.conf";
const char* ALIEN_BETA_CONFIG = "./configuration/alien/alienBeta.conf";
const char* ALIEN_ALFA_CONFIG = "./configuration/alien/alienAlfa.conf";
const char* BRIDGE1_CONFIG = 	"./configuration/bridge/bridge1.conf";
const char* BRIDGE2_CONFIG = 	"./configuration/bridge/bridge2.conf";
const char* BRIDGE3_CONFIG = 	"./configuration/bridge/bridge3.conf";

int screen = 0; // Cristhian Rojas: 26072020
int mode = 0; // Cristhian Rojas: 26072020
 

//struct Alien *aliens[num_aliens]; // Aliens structs array //jrivera 20200724
int load_speed_base(); //jrivera 20200724
int load_normal_weight(); //jrivera 20200724
int load_beta_max_time(); //jrivera 20200724
int load_beta_weight(); //jrivera 20200724
int load_alfa_weight(); //jrivera 20200724
int load_bridge1_algorithm();
int load_bridge2_algorithm();
int load_bridge3_algorithm();
int load_bridge1_candelarizador();
int load_bridge2_candelarizador();
int load_bridge3_candelarizador();
int load_bridge1_weight();
int load_bridge2_weight();
int load_bridge3_weight();
void * AlgorithmsThread(void *arg);

int randint(int n); //jrivera 20200724
struct Alien autoGenerateAliens(int speed_base, int beta_max_time, int normal_weight, int alfa_weight,int beta_weight, int count_id, int, int); //jrivera 20200724
void printMatrix(int[][size]);
void drawMap(int[][size]);
void drawAliens();
void LoadMap(const char *, int[][size]);
char* itoa(int val, int base);
void drawStatistics(int, int, int, int, int, int, int, int);
void * moveAlien(void*);


ALLEGRO_BITMAP *alienN = NULL;
ALLEGRO_BITMAP *alienA = NULL;
ALLEGRO_BITMAP *alienB = NULL;
ALLEGRO_BITMAP *alienI = NULL;

ALLEGRO_FONT *font = NULL;
ALLEGRO_FONT *font2 = NULL; // Cristhian Rojas: 26072020

int done = 0;


int main(int argc, char *argv[])
{
	int speed_base = load_speed_base();
	int beta_max_time = load_beta_max_time();
	int normal_weight = load_normal_weight();
	int beta_weight = load_beta_weight();
	int alfa_weight = load_alfa_weight();
	bridge1_algorithm = load_bridge1_algorithm();
	bridge2_algorithm = load_bridge2_algorithm();
	bridge3_algorithm = load_bridge3_algorithm();
	bridge1_candelarizador = load_bridge1_candelarizador();
	bridge2_candelarizador = load_bridge2_candelarizador();
	bridge3_candelarizador = load_bridge3_candelarizador();
	bridge1_weight = load_bridge1_weight();
	bridge2_weight = load_bridge2_weight();
	bridge3_weight = load_bridge3_weight();
	double start_time_block, block_time;
	double secondsToWait = 0.0;
	double accomulated_time = 0.0;
	printf("speed: %d\n",speed_base);
	printf("beta_max_time: %d\n",beta_max_time);
	printf("normal_weight: %d\n",normal_weight);
	printf("beta_weight: %d\n",beta_weight);
	printf("alfa_weght: %d\n",alfa_weight);

	for(int row =0; row<33; row++){
		for(int column =0; column<50; column++){
			posMat[row][column] = 0;
		}
	} //jrivera 20200724

	for(int i = 0; i<num_aliens;i++){
		aliensFlags[i]=0;
	} //jrivera 20200724

/* 	for(int i = 0; i<num_aliens;i++){
		aliensM[i]->alienID=1;
	} //jrivera 20200724 */
	


	ALLEGRO_DISPLAY *display = NULL;
	ALLEGRO_EVENT_QUEUE *event_queue = NULL;
	ALLEGRO_TIMER *timer = NULL;

	bool running = true;
	bool redraw = true;


	// Initialize allegro
	if (!al_init()) {
		fprintf(stderr, "Failed to initialize allegro.\n");
		return 1;
	}

	// Initialize the timer
	timer = al_create_timer(1.0 / FPS);
	if (!timer) {
		fprintf(stderr, "Failed to create timer.\n");
		return 1;
	}

	// Create the display
	display = al_create_display(screenW, screenH);
	if (!display) {
		fprintf(stderr, "Failed to create display.\n");
		return 1;
	}

	// Create the event queue
	event_queue = al_create_event_queue();
	if (!event_queue) {
		fprintf(stderr, "Failed to create event queue.");
		return 1;
	}

	al_init_primitives_addon();
	al_init_image_addon();
	al_init_font_addon(); // Cristhian Rojas: 26072020
	al_init_ttf_addon(); // Cristhian Rojas: 26072020

	al_install_mouse(); // Cristhian Rojas: 26072020
	al_install_keyboard(); // Cristhian Rojas: 26072020

    font = al_load_font("fonts/space.ttf", 12, 1); // Cristhian Rojas: 26072020
	font2 = al_load_font("fonts/space.ttf", 24, 1); // Cristhian Rojas: 26072020

	ALLEGRO_COLOR red = al_map_rgb(255,0,0); // Cristhian Rojas: 26072020
	ALLEGRO_COLOR blue= al_map_rgb(0,0,255);
	ALLEGRO_COLOR white = al_map_rgb(255,255,255);
	ALLEGRO_COLOR green = al_map_rgb(0,255,0);
	ALLEGRO_COLOR purple = al_map_rgb(100,0,200);
	ALLEGRO_COLOR magenta = al_map_rgb(250,0,214);
	ALLEGRO_COLOR orange = al_map_rgb(255,255,0);

	ALLEGRO_COLOR auto_b = al_map_rgb(0,0,0);
	ALLEGRO_COLOR auto_t = al_map_rgb(0,0,0);
	ALLEGRO_COLOR manu_b = al_map_rgb(0,0,0);
	ALLEGRO_COLOR manu_t = al_map_rgb(0,0,0); // Cristhian Rojas: 26072020

	// Register event sources
	al_register_event_source(event_queue, al_get_display_event_source(display));
	al_register_event_source(event_queue, al_get_timer_event_source(timer));
	al_register_event_source(event_queue, al_get_mouse_event_source()); // Cristhian Rojas: 26072020
	al_register_event_source(event_queue, al_get_keyboard_event_source()); // Cristhian Rojas: 26072020

	// Display a black screen
	al_clear_to_color(al_map_rgb(0, 0, 0));
	al_flip_display();

	// Start the timer
	al_start_timer(timer);

	LoadMap("map.txt", map);

	alienN = al_load_bitmap("images/alienN.png");
	alienA = al_load_bitmap("images/alienA.png");
	alienB = al_load_bitmap("images/alienB.png");
	alienI = al_load_bitmap("images/alienI.png");

	int normals_count = 0;
	int alfas_count = 0;
	int betas_count = 0;
	int intruders_count = 0;

	int arrA = 0;
	int arrB = 0;
	int deleted = 0;

	struct Alien *result = NULL; // Aliens thread result struct pointer

	struct Alien generated[num_aliens];
	struct Alien manually[num_aliens]; // Cristhian Rojas: 26072020

	int count_aliens_generated = 1;
	int count_wait_time = 0;
	int flag_time = 1;
	

	//GENERACION AUTOMATICA
	for(int i= 0; i<num_aliens; i++){
		generated[i] = autoGenerateAliens(speed_base, beta_max_time, normal_weight, alfa_weight, beta_weight, i+1, 2, 3);
		aliens[i] = &generated[i];
		aliensM[i] = &generated[i];
	}
	//----------------------------

	pthread_t  bridge_threads[3];
	//-------------------------------------------
    struct parametros bridge1;
    bridge1.size=3;
    bridge1.bridge='1';
    bridge1.cant=3; 
    bridge1.timeN=2; 
    bridge1.timeS=3;
    bridge1.algorithm=bridge1_algorithm; 
    bridge1.BrigdeWeight=bridge1_weight; 
    bridge1.scheduler=bridge1_candelarizador;
	//--------------------------------------------
	struct parametros bridge2;
    bridge2.size=3;
    bridge2.bridge='2';
    bridge2.cant=3; 
    bridge2.timeN=2; 
    bridge2.timeS=3;
    bridge2.algorithm=bridge2_algorithm; 
    bridge2.BrigdeWeight=bridge2_weight; 
    bridge2.scheduler=bridge2_candelarizador;
	//--------------------------------------------
	struct parametros bridge3;
    bridge3.size=3;
    bridge3.bridge='3';
    bridge3.cant=3; 
    bridge3.timeN=2; 
    bridge3.timeS=3;
    bridge3.algorithm=bridge3_algorithm; 
    bridge3.BrigdeWeight=bridge3_weight; 
    bridge3.scheduler=bridge3_candelarizador;
	//--------------------------------------------

    pthread_create(&bridge_threads[0], NULL , AlgorithmsThread , &bridge1);
	pthread_create(&bridge_threads[1], NULL , AlgorithmsThread , &bridge2);
	pthread_create(&bridge_threads[2], NULL , AlgorithmsThread , &bridge3);
	
	int manual_comm = 2;

	// Game loop
	
	while (running) {
		//printf("MODE: %i\n", mode);
		start_time_block = clock();
		if(flag_time == 1){secondsToWait = possion(6); flag_time = 0; /* printf("time_wait: %lf\n", secondsToWait); */}// jrivera 20200725 
		ALLEGRO_EVENT event;
		ALLEGRO_TIMEOUT timeout;

		// Initialize timeout
		al_init_timeout(&timeout, 0.06);

		
		// Fetch the event (if one exists)
		bool get_event = al_wait_for_event_until(event_queue, &event, &timeout);

		if(event.type == ALLEGRO_EVENT_MOUSE_BUTTON_DOWN) //Cristhian Rojas 26072020
		{
			if(mode == 1)
			{
				for(int element = 0; element < count_aliens_generated; element++)
				{
					if(event.mouse.x <= aliens[element]->x + 10 & event.mouse.x >= aliens[element]->x - 10
						& event.mouse.y <= aliens[element]->y + 10 & event.mouse.y >= aliens[element]->y - 10)
					{
						if(event.mouse.button & 1 & mode != 0)
						{
							aliens[element]->bridge_pass = 7;
							posMat[aliens[element]->row][aliens[element]->column]=0;
						}		
					}
				}
			} 
			else
			{
				for(int element = 0; element < count_aliens_manual; element++)
				{
					if(event.mouse.x <= aliensM[element]->x + 10 & event.mouse.x >= aliensM[element]->x - 10
						& event.mouse.y <= aliensM[element]->y + 10 & event.mouse.y >= aliensM[element]->y - 10)
					{
						if(event.mouse.button & 1 & mode != 0)
						{
							aliensM[element]->bridge_pass = 7;
							posMat[aliensM[element]->row][aliensM[element]->column]=0;
						}		
					}
				}
			}
				
		} //Cristhian Rojas 26072020
			

		if(event.type == ALLEGRO_EVENT_KEY_DOWN)
		{
			switch (event.keyboard.keycode)
			{
			case ALLEGRO_KEY_1:
				manual_comm = 0;
			break;
			case ALLEGRO_KEY_2:
				manual_comm = 1;
			break;
			case ALLEGRO_KEY_3:
				manual_comm = 2;
			break;
			case ALLEGRO_KEY_A:
				if(manual_comm == 0)
				{
					manually[count_aliens_manual] = autoGenerateAliens(speed_base, beta_max_time, normal_weight, alfa_weight, beta_weight, count_aliens_manual+1, 0, 1);
				}
				if(manual_comm == 1)
				{
					manually[count_aliens_manual] = autoGenerateAliens(speed_base, beta_max_time, normal_weight, alfa_weight, beta_weight, count_aliens_manual+1, 1, 1);
				}
				if(manual_comm == 2)
				{
					manually[count_aliens_manual] = autoGenerateAliens(speed_base, beta_max_time, normal_weight, alfa_weight, beta_weight, count_aliens_manual+1, 2, 3);
				}
				aliensM[count_aliens_manual] = &manually[count_aliens_manual];
				count_aliens_manual++;
				break;
			case ALLEGRO_KEY_B:
				if(manual_comm == 0)
				{
					manually[count_aliens_manual] = autoGenerateAliens(speed_base, beta_max_time, normal_weight, alfa_weight, beta_weight, count_aliens_manual+1, 0, 2);
				}
				if(manual_comm == 1)
				{
					manually[count_aliens_manual] = autoGenerateAliens(speed_base, beta_max_time, normal_weight, alfa_weight, beta_weight, count_aliens_manual+1, 1, 2);
				}
				if(manual_comm == 2)
				{
					manually[count_aliens_manual] = autoGenerateAliens(speed_base, beta_max_time, normal_weight, alfa_weight, beta_weight, count_aliens_manual+1, 2, 3);
				}
				aliensM[count_aliens_manual] = &manually[count_aliens_manual];
				count_aliens_manual++;
				break;
			case ALLEGRO_KEY_N:
				if(manual_comm == 0)
				{
					manually[count_aliens_manual] = autoGenerateAliens(speed_base, beta_max_time, normal_weight, alfa_weight, beta_weight, count_aliens_manual+1, 0, 0);
				}
				if(manual_comm == 1)
				{
					manually[count_aliens_manual] = autoGenerateAliens(speed_base, beta_max_time, normal_weight, alfa_weight, beta_weight, count_aliens_manual+1, 1, 0);
				}
				if(manual_comm == 2)
				{
					manually[count_aliens_manual] = autoGenerateAliens(speed_base, beta_max_time, normal_weight, alfa_weight, beta_weight, count_aliens_manual+1, 2, 3);
				}
				aliensM[count_aliens_manual] = &manually[count_aliens_manual];
				count_aliens_manual++;
				break;	
			case ALLEGRO_KEY_E:
				screen = 2;
				break;
			case ALLEGRO_KEY_R:
				for(int i= 0; i<num_aliens; i++)
				{
					posMat[aliens[i]->row][aliens[i]->column]=0;
					generated[i] = autoGenerateAliens(speed_base, beta_max_time, normal_weight, alfa_weight, beta_weight, i+1, 2, 3);
					aliens[i] = &generated[i];
				}
				for(int i= 0; i<count_aliens_manual; i++)
				{
					posMat[aliensM[i]->row][aliensM[i]->column]=0;
					aliensM[i] = NULL;
				}
				
				count_aliens_manual = 0;
				normals_count = 0;
				alfas_count = 0;
				betas_count = 0;
				arrB = 0;
				arrA = 0;
				intruders_count = 0;
				done = 0;
				deleted = 0;
				screen = 0;
				break;
			case ALLEGRO_KEY_Q:
				running = 0;
				break;
			default:
				break;
			}
		}

		// Handle the event
		if (get_event) {
			switch (event.type) {
				case ALLEGRO_EVENT_TIMER:
					redraw = true;
					break;
				case ALLEGRO_EVENT_DISPLAY_CLOSE:
					running = false;
					break;
				default:
					/* fprintf(stderr, "Unsupported event received: %d\n", event.type); */
					break;
			}
		}

		// <------------------------------------------- MAIN SCREEN ------------------------------------------->
		// Check if we need to redraw
		if (redraw && al_is_event_queue_empty(event_queue) & screen == 1) { // Cristhian Rojas: 26072020 (screen flag)

			// Redraw
			
			al_clear_to_color(al_map_rgb(30, 15, 35));
			drawMap(map); // Draw the map
			al_draw_text(font, al_map_rgb(255,255,0), 50,220,0, "ALIEN COMMUNITY A");
			al_draw_text(font, al_map_rgb(0,255,0), 810,460,0, "ALIEN COMMUNITY B");
			drawAliens(); // Draw the aliens

			drawStatistics(normals_count, alfas_count, betas_count, intruders_count, arrA, arrB, 0, deleted);

			
			if(mode == 1)
			{
				//printf("DONE: %i\n", done);
				for(int element = 0; element < count_aliens_generated; element++)
				{
				
					if(0 != pthread_create(&aliens[element]->tAlien, NULL, moveAlien, aliens[element]) & aliens[element]->bridge_pass != 5)
					{
						printf("Error: Can not create thread\n.");
						exit(1);
					}
					if(aliens[element]->type == 0 & aliens[element]->bridge_pass != 5 & aliens[element]->count_done == 0)
					{
						normals_count += 1;
						aliens[element]->count_done = 1;
					}
					if(aliens[element]->type == 1 & aliens[element]->bridge_pass != 5 & aliens[element]->count_done == 0) 
					{
						alfas_count += 1;
						aliens[element]->count_done = 1;
					}
					if(aliens[element]->type == 2 & aliens[element]->bridge_pass != 5 & aliens[element]->count_done == 0) 
					{
						betas_count += 1;
						aliens[element]->count_done = 1;
					}
					if(aliens[element]->type == 3 & aliens[element]->bridge_pass != 5 & aliens[element]->count_done == 0) 
					{
						intruders_count += 1;
						aliens[element]->count_done = 1;
					}
					
					pthread_join(aliens[element]->tAlien, (void *)&result);
					
					al_draw_bitmap(result->alien, result->x, result->y, 0);
				
					aliens[element] = result;

					if(accomulated_time >= secondsToWait){
						if(count_aliens_generated < num_aliens){count_aliens_generated++;}
						accomulated_time = 0.0; 
						flag_time = 1;}	

					for(int e = 0; e < count_aliens_generated; e++)
					{
						if(aliens[e]->bridge_pass == 4 | aliens[e]->bridge_pass == 7) 
						{
							if(aliens[e]->bridge_pass == 7) // Cristhian Rojas 27072020
							{
								deleted++;
							}
							else
							{
								if(aliens[e]->community == 0) arrB++;
								else arrA++;
							} // Cristhian Rojas 27072020

							aliens[e]->bridge_pass = 5;
							aliens[e]->x = 2000;
							aliens[e]->y = 2000;
							posMat[(aliens[e]->row)][(aliens[e]->column)] = 0;
							done += 1;
							if(aliens[e]->type == 0) normals_count -= 1;
							if(aliens[e]->type == 1) alfas_count -= 1;
							if(aliens[e]->type == 2) betas_count -= 1;
							if(aliens[e]->type == 3) intruders_count -= 1;
						}
					}
				}

			}
			else
			{

				for(int element = 0; element < count_aliens_manual; element++)
				{
					
					if(0 != pthread_create(&aliensM[element]->tAlien, NULL, moveAlien, aliensM[element]) & aliensM[element]->bridge_pass != 5)
					{
						printf("Error: Can not create thread\n.");
						exit(1);
					}
					if(aliensM[element]->type == 0 & aliensM[element]->bridge_pass != 5 & aliensM[element]->count_done == 0)
					{
						normals_count += 1;
						aliensM[element]->count_done = 1;
					}
					if(aliensM[element]->type == 1 & aliensM[element]->bridge_pass != 5 & aliensM[element]->count_done == 0) 
					{
						alfas_count += 1;
						aliensM[element]->count_done = 1;
					}
					if(aliensM[element]->type == 2 & aliensM[element]->bridge_pass != 5 & aliensM[element]->count_done == 0) 
					{
						betas_count += 1;
						aliensM[element]->count_done = 1;
					}
					if(aliensM[element]->type == 3 & aliensM[element]->bridge_pass != 5 & aliensM[element]->count_done == 0) 
					{
						intruders_count += 1;
						aliensM[element]->count_done = 1;
					}
					
					pthread_join(aliensM[element]->tAlien, (void *)&result);
					
					al_draw_bitmap(result->alien, result->x, result->y, 0);
				
					aliensM[element] = result;	
				}

				for(int e = 0; e < count_aliens_manual; e++)
				{
					if(aliensM[e]->bridge_pass == 4 | aliensM[e]->bridge_pass == 7) 
					{
						if(aliensM[e]->bridge_pass == 7) // Cristhian Rojas 27072020
						{
							deleted++;
						}
						else
						{
							if(aliensM[e]->community == 0) arrB++;
							else arrA++;
						} // Cristhian Rojas 27072020

						aliensM[e]->bridge_pass = 5;
						aliensM[e]->x = 2000;
						aliensM[e]->y = 2000;
						posMat[(aliensM[e]->row)][(aliensM[e]->column)] = 0;

						if(aliensM[e]->type == 0) normals_count -= 1;
						if(aliensM[e]->type == 1) alfas_count -= 1;
						if(aliensM[e]->type == 2) betas_count -= 1;
						if(aliensM[e]->type == 3) intruders_count -= 1;
					}
				}
				al_draw_text(font, white, 780, 260,0, "USER MANUAL GUIDE");
				al_draw_text(font, white, 750, 280,0, "USE (1) TO SELECT A COMMUNITY");
				al_draw_text(font, white, 750, 300,0, "USE (2) TO SELECT B COMMUNITY");
				al_draw_text(font, white, 750, 320,0, "USE (3) TO SELECT RANDOM");
				al_draw_text(font, white, 750, 340,0, "USE (A) TO SELECT ALFA ALIEN");
				al_draw_text(font, white, 750, 360,0, "USE (B) TO SELECT BETA ALIEN");
				al_draw_text(font, white, 750, 380,0, "USE (N) TO SELECT NORMAL ALIEN");
			}

			al_flip_display();
			redraw = false;	
			if(done == num_aliens) screen = 2; //Cristhian Rojas 26072020
	
		}
		// <------------------------------------------- INITIAL SCREEN ------------------------------------------->
		if (redraw && al_is_event_queue_empty(event_queue) & screen == 0)  // Cristhian Rojas: 26072020
		{
			al_clear_to_color(al_map_rgb(30, 15, 35));

			if(event.type == ALLEGRO_EVENT_MOUSE_BUTTON_DOWN)
			{
				if(event.mouse.button & 1 & mode != 0)
					{
						screen = 1;
					}
			}

			if(event.type == ALLEGRO_EVENT_MOUSE_AXES)
			{
				if(event.mouse.x >= screenW/2 - 300 & event.mouse.x <= screenW/2 - 50 & 
				event.mouse.y <= screenH/2 + 50 & event.mouse.y >= screenH/2 - 50)
				{
					auto_b = blue;
					auto_t = white;
					manu_b = magenta;
					manu_t = orange;
					mode = 1;	
				}
				else 
				{
					if(event.mouse.x <= screenW/2 + 350 & event.mouse.x >= screenW/2 + 100 & 
					event.mouse.y <= screenH/2 + 50 & event.mouse.y >= screenH/2 - 50)
					{
						auto_b = purple;
						auto_t = green;
						manu_b = red;
						manu_t = white;
						mode = 2;
					}
					else
					{
						auto_b = purple;
						auto_t = green;
						manu_b = magenta;
						manu_t = orange;
						mode = 0;
					}	
				}

				al_draw_filled_rounded_rectangle(screenW/2 - 300, screenH/2 - 50,screenW/2 - 50,screenH/2 + 50, 
				12, 20, auto_b);
				al_draw_filled_rounded_rectangle(screenW/2 + 100, screenH/2 - 50,screenW/2 + 350,screenH/2 + 50, 
				12, 20, manu_b);
				al_draw_text(font2, auto_t, screenW/2 - 290,screenH/2 -10,0, "AUTOMATIC MODE");
				al_draw_text(font2, manu_t, screenW/2 + 125,screenH/2 -10,0, "MANUAL MODE");
				al_draw_text(font2, white, screenW/2 - 125,screenH/2 -190,0, "USE (R) TO RESET GAME");
				al_draw_text(font2, white, screenW/2 - 125,screenH/2 -160,0, "USE (E) TO END GAME");
				al_draw_text(font2, white, screenW/2 - 125,screenH/2 -130,0, "USE (Q) TO QUIT GAME");	
				al_flip_display();
				redraw = false;
			} // Cristhian Rojas: 26072020
		}

		if (redraw && al_is_event_queue_empty(event_queue) & screen == 2)
			{
				al_clear_to_color(al_map_rgb(30, 15, 35));
				drawStatistics(normals_count, alfas_count, betas_count, intruders_count, arrA, arrB, 1, deleted);
				al_flip_display();
				redraw = false;
			}

		start_time_block = clock() - start_time_block;
        block_time = ((double)start_time_block)/CLOCKS_PER_SEC;
		accomulated_time += (block_time+0.009);
	
       // printf("En la región crítica: %lf\n", block_time);	
	} 
	for(int i =0; i<33; i++){
			for(int j =0; j<50; j++){
				printf("%d",posMat[i][j]);
			}
			printf("\n");
		}
	

	// Clean up
	go = 0;
	al_destroy_display(display);
	al_destroy_event_queue(event_queue);
    al_destroy_timer(timer);
    al_destroy_font(font);
	al_destroy_font(font2); // Cristhian Rojas: 26072020
	al_destroy_bitmap(alienN);
	al_destroy_bitmap(alienA);
	al_destroy_bitmap(alienB);
	al_destroy_bitmap(alienI);
	/* pthread_join(bridge_threads[0], NULL);
	pthread_join(bridge_threads[1], NULL);
	pthread_join(bridge_threads[2], NULL); */

	return 0;
}

void drawStatistics(int nc, int ac, int bc, int ic, int arrA, int arrB, int s, int removed)
{
	char normal[50] = "NORMAL ALIENS: ";
	char alfa[50] = "ALFA ALIENS: ";
	char beta[50] = "BETA ALIENS: ";
	char intruder[50] = "INTRUDER ALIENS: ";

	char arrivedA[50] = "Arrived in Community A: ";
	char arrivedB[50] = "Arrived in Community B: ";
	char deleted[50] = "Eliminated on Road: "; // Cristhian Rojas 27072020

	const char* normals_count = itoa(nc,10);
	strcat(normal, normals_count);
	const char* alfas_count = itoa(ac,10);
	strcat(alfa, alfas_count);
	const char* betas_count = itoa(bc,10);
	strcat(beta, betas_count);
	const char* intruders_count = itoa(ic,10);
	strcat(intruder, intruders_count);
	const char* arrAC = itoa(arrA,10);
	strcat(arrivedA, arrAC);
	const char* arrBC = itoa(arrB,10);
	strcat(arrivedB, arrBC);
	const char* del = itoa(removed,10); // Cristhian Rojas 27072020
	strcat(deleted, del); // Cristhian Rojas 27072020

	
	if(s)
	{
		// al_draw_bitmap(alienN, screenW/2 - 100, screenH/2 - 90, 0);
		// al_draw_bitmap(alienA, screenW/2 - 100, screenH/2 - 60, 0);
		// al_draw_bitmap(alienB, screenW/2 - 100, screenH/2 - 30, 0);
		// al_draw_bitmap(alienI, screenW/2 - 100, screenH/2, 0);
		// al_draw_text(font, al_map_rgb(0,255,0), screenW/2 - 75,screenH/2 - 90,0, normal);
		// al_draw_text(font, al_map_rgb(0,0,255), screenW/2 - 75,screenH/2 - 60,0, alfa);
		// al_draw_text(font, al_map_rgb(255,130,0), screenW/2 - 75,screenH/2 - 30,0, beta);
		// al_draw_text(font, al_map_rgb(255,0,0), screenW/2 - 75,screenH/2,0, intruder);
		al_draw_text(font, al_map_rgb(0,255,0) , screenW/2 - 25,screenH/2 -100,0, "GAME OVER!");
		al_draw_text(font, al_map_rgb(250,0,215) , screenW/2 - 75,screenH/2 - 70 ,0, arrivedA);
		al_draw_text(font, al_map_rgb(100,0,200), screenW/2 - 75,screenH/2 - 40,0, arrivedB);
		al_draw_text(font, al_map_rgb(255,0,0), screenW/2 - 75,screenH/2 + -10,0, deleted); // Cristhian Rojas 27072020
	}
	else
	{
		al_draw_text(font, al_map_rgb(255,255,255), 720,20,0, "RUNNING ON MAP");
		al_draw_bitmap(alienN, 700, 50, 0);
		al_draw_bitmap(alienA, 700, 80, 0);
		al_draw_bitmap(alienB, 700, 110, 0);
		al_draw_bitmap(alienI, 700, 140, 0);
		al_draw_text(font, al_map_rgb(255,255,255), 740,50,0, normal);
		al_draw_text(font, al_map_rgb(255,255,255), 740,80,0, alfa);
		al_draw_text(font, al_map_rgb(255,255,255), 740,110,0, beta);
		al_draw_text(font, al_map_rgb(255,255,255), 740,140,0, intruder);
		al_draw_text(font, al_map_rgb(255,255,255), 50,400,0, arrivedA);
		al_draw_text(font, al_map_rgb(255,255,255), 50,450,0, arrivedB);
		al_draw_text(font, al_map_rgb(255,255,255), 50,500,0, deleted); // Cristhian Rojas 27072020
	}	
}


void * moveAlien(void * arg)
{
	struct Alien *alien = (struct Alien*)arg;
	struct Alien *alienResult = (struct Alien*)malloc(sizeof(struct Alien));
	int internal_flag = 1;


	if(alien->community == 0)
	{
		if(alien->y >= 80 & alien->x == 140 & alien->bridge_pass == 0 & posMat[(alien->row)-1][(alien->column)] == 0)
		{
			alien->y -= alien->moveSpeed;
			alien->cont += alien->moveSpeed;
			alien->mapPos = 1;
			
			if(alien->cont >= 20) 
			{
				posMat[(alien->row)][(alien->column)] = 0;
				alien->row -= 1;
				posMat[alien->row][alien->column] = alien->alienID;
				alien->cont = 0;
				alien->mapPos = 0;
			}
		}
		if(alien->y <= 80 & alien->x <= 380 & alien->bridge_pass == 0 & posMat[(alien->row)][(alien->column)+1] == 0)
		{
			alien->x += alien->moveSpeed;
			alien->cont += alien->moveSpeed;
			alien->mapPos = 1;
			
			if(alien->cont >= 20) 
			{
				posMat[(alien->row)][(alien->column)] = 0;
				alien->column += 1;
				posMat[alien->row][alien->column] = alien->alienID;
				alien->cont = 0;
				alien->mapPos = 0;
			}
		}
		if(alien->y <= 160 & alien->x >= 380 & alien->bridge_pass == 0 & posMat[(alien->row)+1][(alien->column)] == 0)
		{
			alien->y += alien->moveSpeed;
			alien->cont += alien->moveSpeed;
			alien->mapPos = 1;
			
			if(alien->cont >= 20) 
			{
				posMat[(alien->row)][(alien->column)] = 0;
				alien->row += 1;
				posMat[alien->row][alien->column] = alien->alienID;
				alien->cont = 0;
				alien->mapPos = 0;
			}
		}
		if(alien->y >= 160)
		{
			if(alien->bridge_ID == 'c')
			{
				if(alien->x >= 380 & alien->y <= 180 & alien->bridge_pass == 0 & posMat[(alien->row)+1][(alien->column)] == 0)
				{
					
					alien->y += alien->moveSpeed;
					alien->cont += alien->moveSpeed;
					alien->mapPos = 1;
					
					if(alien->cont >= 20) 
					{
						posMat[(alien->row)][(alien->column)] = 0;
						alien->row += 1;
						posMat[alien->row][alien->column] = alien->alienID;
						alien->cont = 0;
						alien->mapPos = 0;
					}
				}
				if(alien->x >= 380 & alien->y <= 200 & alien->bridge_pass == 0 & posMat[(alien->row)+1][(alien->column)] == 0)
				{
					
					alien->y += alien->moveSpeed;
					alien->cont += alien->moveSpeed;
					alien->mapPos = 1;
					
					if(alien->cont >= 20) 
					{
						posMat[(alien->row)][(alien->column)] = 0;
						alien->row += 1;
						posMat[alien->row][alien->column] = alien->alienID;
						alien->cont = 0;
						alien->mapPos = 0;
					}
				}
				if(alien->x <= 480 & alien->y >= 200 & alien->bridge_pass == 0 & posMat[(alien->row)][(alien->column)+1] == 0)
				{
					alien->x += alien->moveSpeed;
					alien->cont += alien->moveSpeed;
					alien->mapPos = 1;
					
					if(alien->cont >= 20) 
					{
						posMat[(alien->row)][(alien->column)] = 0;
						alien->column += 1;
						posMat[alien->row][alien->column] = alien->alienID;
						alien->cont = 0;
						alien->mapPos = 0;
					}
				}
				if(alien->x >= 480 & alien->y <= 260 & alien->bridge_pass == 0 & posMat[(alien->row)+1][(alien->column)] == 0 ) 
				{
					alien->y += alien->moveSpeed;
					if(alien->y >= 258) alien->bridge_pass = 2;
					alien->cont += alien->moveSpeed;
					alien->mapPos = 1;
					
					if(alien->cont >= 20) 
					{
						posMat[(alien->row)][(alien->column)] = 0;
						alien->row += 1;
						posMat[alien->row][alien->column] = alien->alienID;
						alien->cont = 0;
						alien->mapPos = 0;
					}
				}
				if(alien->x >= 480 & alien->y <= 460 & alien->bridge_pass == 2) // Waiting for algorithms
				{
					if(bridge2_algorithm == 3){
						if(posMat[(alien->row)+1][(alien->column)] == 0 & posMat[(alien->row)+2][(alien->column)] == 0 & posMat[(alien->row)+3][(alien->column)] == 0 & posMat[(alien->row)+4][(alien->column)] == 0 & posMat[(alien->row)+5][(alien->column)] == 0 & posMat[(alien->row)+6][(alien->column)] == 0 /* & posMat[(alien->row)+6][(alien->column)+1] == 0 */){
							alien->y += alien->moveSpeed;
							if(alien->y >= 460) alien->bridge_pass = 1;
							alien->cont += alien->moveSpeed;
							alien->mapPos = 1;
							
							if(alien->cont >= 20) 
							{
								posMat[(alien->row)][(alien->column)] = 0;
								alien->row += 1;
								posMat[alien->row][alien->column] = alien->alienID;
								alien->cont = 0;
								alien->mapPos = 0;
							}

						}

					}
					else{
						if(aliensFlags[(alien->alienID)-1] == 1){
							posMat[(alien->row)][(alien->column)] = 0;
							alien->x = 480;
							alien->y = 280;
							alien->row = 14;
							alien->column = 24;
							aliensFlags[(alien->alienID)-1] =  2;
						}
						if(aliensFlags[(alien->alienID)-1] == 2){
							
							if(posMat[(alien->row)+1][(alien->column)] == 0) // Waiting for algorithms
							{
								alien->y += alien->moveSpeed;
								if(alien->y >= 460) alien->bridge_pass = 1;
								alien->cont += alien->moveSpeed;
								alien->mapPos = 1;
								
								if(alien->cont >= 20) 
								{
									posMat[(alien->row)][(alien->column)] = 0;
									alien->row += 1;
									posMat[alien->row][alien->column] = alien->alienID;
									alien->cont = 0;
									alien->mapPos = 0;
								}
							}
						}
	
					}
				}
				if(alien->x >= 480 & alien->y >= 460 & alien->bridge_pass == 2 & posMat[(alien->row)+1][(alien->column)] == 0) 
				{
					alien->y += alien->moveSpeed;
					if(alien->y >= 480) alien->bridge_pass = 1;
					alien->cont += alien->moveSpeed;
					alien->mapPos = 1;
					if(alien->cont >= 20) 
					{
						posMat[(alien->row)][(alien->column)] = 0;
						//printf("contador_pixeles: %f\n",alien->cont);
						alien->row += 1;
						posMat[alien->row][alien->column] = alien->alienID;
						
						alien->cont = 0;
						alien->mapPos = 0;
					}
				}
				if(alien->x >= 440 & alien->y >= 460 & alien->bridge_pass == 1 & posMat[(alien->row)][(alien->column)-1] == 0) 
				{
					alien->x -= alien->moveSpeed;
					alien->cont += alien->moveSpeed;
					alien->mapPos = 1;
					
					if(alien->cont >= 20) 
					{
						posMat[(alien->row)][(alien->column)] = 0;
						//printf("contador_pixeles: %f\n",alien->cont);
						alien->column -= 1;
						posMat[alien->row][alien->column] = alien->alienID;
						alien->cont = 0;
						alien->mapPos = 0;
					}
				}
				if(alien->x <= 440 & alien->y <= 620 & alien->bridge_pass == 1 & posMat[(alien->row)+1][(alien->column)] == 0) 
				{
					alien->y += alien->moveSpeed;
					if(alien->y >= 620) alien->bridge_pass = 3;
					alien->cont += alien->moveSpeed;
					alien->mapPos = 1;
					
					if(alien->cont >= 20) 
					{
						posMat[(alien->row)][(alien->column)] = 0;
						//printf("contador_pixeles: %f\n",alien->cont);
						alien->row += 1;
						posMat[alien->row][alien->column] = alien->alienID;
						alien->cont = 0;
						alien->mapPos = 0;
					}
				}
				if(alien->x <= 920 & alien->y >= 620 & alien->bridge_pass == 3 & posMat[(alien->row)][(alien->column)+1] == 0) 
				{
					alien->x += alien->moveSpeed;
					alien->cont += alien->moveSpeed;
					alien->mapPos = 1;
					
					if(alien->cont >= 20) 
					{
						posMat[(alien->row)][(alien->column)] = 0;
						//printf("contador_pixeles: %f\n",alien->cont);
						alien->column += 1;
						posMat[alien->row][alien->column] = alien->alienID;
						alien->cont = 0;
						alien->mapPos = 0;
					}
				}
				if(alien->x >= 920 & alien->y >= 500 & alien->bridge_pass == 3 & posMat[(alien->row)-1][(alien->column)] == 0) 
				{
					alien->y -= alien->moveSpeed;
					if(alien->y <= 500) alien->bridge_pass = 4;
					alien->cont += alien->moveSpeed;
					alien->mapPos = 1;
					
					if(alien->cont >= 20) 
					{
						posMat[(alien->row)][(alien->column)] = 0;
						//printf("contador_pixeles: %f\n",alien->cont);
						alien->row -= 1;
						posMat[alien->row][alien->column] = alien->alienID;
						alien->cont = 0;
						alien->mapPos = 0;
					}
				}

			}
			if(alien->bridge_ID == 'w')
			{
				if(alien->x >= 380 & alien->y <= 200 & alien->bridge_pass == 0 & posMat[(alien->row)+1][(alien->column)] == 0)
				{
					alien->y += alien->moveSpeed;
					alien->cont += alien->moveSpeed;
					alien->mapPos = 1;
					
					if(alien->cont >= 20) 
					{
						posMat[(alien->row)][(alien->column)] = 0;
						//printf("contador_pixeles: %f\n",alien->cont);
						alien->row += 1;
						posMat[alien->row][alien->column] = alien->alienID;
						alien->cont = 0;
						alien->mapPos = 0;
					}
				}
				if(alien->x <= 640 & alien->y >= 200 & alien->bridge_pass == 0 & posMat[(alien->row)][(alien->column)+1] == 0)
				{
					alien->x += alien->moveSpeed;
					alien->cont += alien->moveSpeed;
					alien->mapPos = 1;
					
					if(alien->cont >= 20) 
					{
						posMat[(alien->row)][(alien->column)] = 0;
						//printf("contador_pixeles: %f\n",alien->cont);
						alien->column += 1;
						posMat[alien->row][alien->column] = alien->alienID;
						alien->cont = 0;
						alien->mapPos = 0;
					}
				}
				if(alien->x >= 640 & alien->y <= 258 & alien->bridge_pass == 0 & posMat[(alien->row)+1][(alien->column)] == 0) 
				{
					alien->y += alien->moveSpeed;
					if(alien->y >= 258) alien->bridge_pass = 2;
					alien->cont += alien->moveSpeed;
					alien->mapPos = 1;
					
					if(alien->cont >= 20) 
					{
						posMat[(alien->row)][(alien->column)] = 0;
						//printf("contador_pixeles: %f\n",alien->cont);
						alien->row += 1;
						posMat[alien->row][alien->column] = alien->alienID;
						alien->cont = 0;
						alien->mapPos = 0;
					}
				}
				if(alien->x >= 640 & alien->y <= 460 & alien->bridge_pass == 2) // Waiting for algorithms
				{
					if(bridge3_algorithm == 3){
						if(posMat[(alien->row)+1][(alien->column)] == 0 & posMat[(alien->row)+2][(alien->column)] == 0 & posMat[(alien->row)+3][(alien->column)] == 0 & posMat[(alien->row)+4][(alien->column)] == 0 & posMat[(alien->row)+5][(alien->column)] == 0 & posMat[(alien->row)+6][(alien->column)] == 0 /* & posMat[(alien->row)+6][(alien->column)+1] == 0 */){
							alien->y += alien->moveSpeed;
							if(alien->y >= 460) alien->bridge_pass = 1;
							alien->cont += alien->moveSpeed;
							alien->mapPos = 1;
							
							if(alien->cont >= 20) 
							{
								posMat[(alien->row)][(alien->column)] = 0;
								//printf("contador_pixeles: %f\n",alien->cont);
								alien->row += 1;
								posMat[alien->row][alien->column] = alien->alienID;
								alien->cont = 0;
								alien->mapPos = 0;
							}

						}

					}
					else{
						if(aliensFlags[(alien->alienID)-1] == 1){
							posMat[(alien->row)][(alien->column)] = 0;
							alien->x = 640;
							alien->y = 280;
							alien->row = 14;
							alien->column = 32;
							aliensFlags[(alien->alienID)-1] = 2;
						}
						if(aliensFlags[(alien->alienID)-1] == 2){
							
							if(posMat[(alien->row)+1][(alien->column)] == 0) // Waiting for algorithms
							{
								alien->y += alien->moveSpeed;
								if(alien->y >= 460) alien->bridge_pass = 1;
								alien->cont += alien->moveSpeed;
								alien->mapPos = 1;
								
								if(alien->cont >= 20) 
								{
									posMat[(alien->row)][(alien->column)] = 0;
									alien->row += 1;
									posMat[alien->row][alien->column] = alien->alienID;
									alien->cont = 0;
									alien->mapPos = 0;
								}
								
							}
						}
					}

				}
					
					
				
				
				if(alien->x >= 440 & alien->y >= 460 & alien->bridge_pass == 1 & posMat[(alien->row)][(alien->column)-1] == 0) 
				{
					alien->x -= alien->moveSpeed;
					alien->cont += alien->moveSpeed;
					alien->mapPos = 1;
					
					if(alien->cont >= 20) 
					{
						posMat[(alien->row)][(alien->column)] = 0;
						//printf("contador_pixeles: %f\n",alien->cont);
						alien->column -= 1;
						posMat[alien->row][alien->column] = alien->alienID;
						alien->cont = 0;
						alien->mapPos = 0;
					}
				}
				if(alien->x <= 440 & alien->y <= 620 & alien->bridge_pass == 1 & posMat[(alien->row)+1][(alien->column)] == 0) 
				{
					alien->y += alien->moveSpeed;
					if(alien->y >= 620) alien->bridge_pass = 3;
					alien->cont += alien->moveSpeed;
					alien->mapPos = 1;
					
					if(alien->cont >= 20) 
					{
						posMat[(alien->row)][(alien->column)] = 0;
						//printf("contador_pixeles: %f\n",alien->cont);
						alien->row += 1;
						posMat[alien->row][alien->column] = alien->alienID;
						alien->cont = 0;
						alien->mapPos = 0;
					}
				}
				if(alien->x <= 920 & alien->y >= 620 & alien->bridge_pass == 3 & posMat[(alien->row)][(alien->column)+1] == 0) 
				{
					alien->x += alien->moveSpeed;
					alien->cont += alien->moveSpeed;
					alien->mapPos = 1;
					
					if(alien->cont >= 20) 
					{
						posMat[(alien->row)][(alien->column)] = 0;
						//printf("contador_pixeles: %f\n",alien->cont);
						alien->column += 1;
						posMat[alien->row][alien->column] = alien->alienID;
						alien->cont = 0;
						alien->mapPos = 0;
					}
				}
				if(alien->x >= 920 & alien->y >= 500 & alien->bridge_pass == 3 & posMat[(alien->row)-1][(alien->column)] == 0) 
				{
					alien->y -= alien->moveSpeed;
					if(alien->y <= 500) alien->bridge_pass = 4;
					alien->cont += alien->moveSpeed;
					alien->mapPos = 1;
					
					if(alien->cont >= 20) 
					{
						posMat[(alien->row)][(alien->column)] = 0;
						//printf("contador_pixeles: %f\n",alien->cont);
						alien->row -= 1;
						posMat[alien->row][alien->column] = alien->alienID;
						alien->cont = 0;
						alien->mapPos = 0;
					}
				}
			}
			if(alien->bridge_ID == 'e')
			{
				if(alien->x >= 380 & alien->y <= 180 & alien->bridge_pass == 0 & posMat[(alien->row)+1][(alien->column)] == 0)
				{
					alien->y += alien->moveSpeed;
					alien->cont += alien->moveSpeed;
					alien->mapPos = 1;
					
					if(alien->cont >= 20) 
					{
						posMat[(alien->row)][(alien->column)] = 0;
						//printf("contador_pixeles: %f\n",alien->cont);
						alien->row += 1;
						posMat[alien->row][alien->column] = alien->alienID;
						alien->cont = 0;
						alien->mapPos = 0;
					}
				}
				if(alien->x >= 320 & alien->y >= 180 & alien->bridge_pass == 0 & posMat[(alien->row)][(alien->column)-1] == 0)
				{
					alien->x -= alien->moveSpeed;
					alien->cont += alien->moveSpeed;
					alien->mapPos = 1;
					
					if(alien->cont >= 20) 
					{
						posMat[(alien->row)][(alien->column)] = 0;
						//printf("contador_pixeles: %f\n",alien->cont);
						alien->column -= 1;
						posMat[alien->row][alien->column] = alien->alienID;
						alien->cont = 0;
						alien->mapPos = 0;
					}
				}
				if(alien->x <= 320 & alien->y <= 260 & alien->bridge_pass == 0 & posMat[(alien->row)+1][(alien->column)] == 0) 
				{
					alien->y += alien->moveSpeed;
					if(alien->y >= 258) alien->bridge_pass = 2;
					alien->cont += alien->moveSpeed;
					alien->mapPos = 1;
					
					if(alien->cont >= 20) 
					{
						posMat[(alien->row)][(alien->column)] = 0;
						//printf("contador_pixeles: %f\n",alien->cont);
						alien->row += 1;
						posMat[alien->row][alien->column] = alien->alienID;
						alien->cont = 0;
						alien->mapPos = 0;
					}
				}
				if(alien->x <= 320 & alien->y <= 260 & alien->bridge_pass == 2 & posMat[(alien->row)+1][(alien->column)] == 0) // Waiting for algorithms
				{
					alien->y += alien->moveSpeed;
							
					alien->cont += alien->moveSpeed;
					alien->mapPos = 1;
					
					if(alien->cont >= 20) 
					{
						posMat[(alien->row)][(alien->column)] = 0;
						//printf("contador_pixeles: %f\n",alien->cont);
						alien->row += 1;
						posMat[alien->row][alien->column] = alien->alienID;
						alien->cont = 0;
						alien->mapPos = 0;
					}
					
				}
				if(alien->x <= 320 & alien->y >= 260 & alien->bridge_pass == 2) // Waiting for algorithms
				{
					if(bridge1_algorithm == 3){
						if(posMat[(alien->row)+6][(alien->column)] == 0 & posMat[(alien->row)+1][(alien->column)] == 0 & posMat[(alien->row)+2][(alien->column)] == 0 & posMat[(alien->row)+3][(alien->column)] == 0 & posMat[(alien->row)+4][(alien->column)] == 0 & posMat[(alien->row)+5][(alien->column)] == 0  /* & posMat[(alien->row)+6][(alien->column)+1] == 0 */){
							alien->y += alien->moveSpeed;
							if(alien->y >= 480) alien->bridge_pass = 1;
							alien->cont += alien->moveSpeed;
							alien->mapPos = 1;
							
							if(alien->cont >= 20) 
							{
								posMat[(alien->row)][(alien->column)] = 0;
								//printf("contador_pixeles: %f\n",alien->cont);
								alien->row += 1;
								posMat[alien->row][alien->column] = alien->alienID;
								alien->cont = 0;
								alien->mapPos = 0;
							}

						}

					}
					else{
						if(aliensFlags[(alien->alienID)-1] == 1){
							posMat[(alien->row)][(alien->column)] = 0;
							alien->x = 320;
							alien->y = 280;
							alien->row = 14;
							alien->column = 16;
							aliensFlags[(alien->alienID)-1] = 2;
						}
						
						if(aliensFlags[(alien->alienID)-1] == 2){	
							if(posMat[(alien->row)+1][(alien->column)] == 0) // Waiting for algorithms
							{
								alien->y += alien->moveSpeed;
								if(alien->y >= 480) alien->bridge_pass = 1;
								alien->cont += alien->moveSpeed;
								alien->mapPos = 1;
								
								if(alien->cont >= 20) 
								{
									posMat[(alien->row)][(alien->column)] = 0;
									alien->row += 1;
									posMat[alien->row][alien->column] = alien->alienID;
									alien->cont = 0;
									alien->mapPos = 0;
								}
								
							}
						}

					}
					
				}
				if(alien->x <= 440 & alien->y >= 480 & alien->bridge_pass == 1 & posMat[(alien->row)][(alien->column)+1] == 0) 
				{
					alien->x += alien->moveSpeed;
					alien->cont += alien->moveSpeed;
					alien->mapPos = 1;
					
					if(alien->cont >= 20) 
					{
						posMat[(alien->row)][(alien->column)] = 0;
						//printf("contador_pixeles: %f\n",alien->cont);
						alien->column += 1;
						posMat[alien->row][alien->column] = alien->alienID;
						alien->cont = 0;
						alien->mapPos = 0;
					}
				}
				if(alien->x >= 440 & alien->y <= 620 & alien->bridge_pass == 1 & posMat[(alien->row)+1][(alien->column)] == 0) 
				{
					alien->y += alien->moveSpeed;
					if(alien->y >= 620) alien->bridge_pass = 3;
					alien->cont += alien->moveSpeed;
					alien->mapPos = 1;
					
					if(alien->cont >= 20) 
					{
						posMat[(alien->row)][(alien->column)] = 0;
						//printf("contador_pixeles: %f\n",alien->cont);
						alien->row += 1;
						posMat[alien->row][alien->column] = alien->alienID;
						alien->cont = 0;
						alien->mapPos = 0;
					}
				}
				if(alien->x <= 920 & alien->y >= 620 & alien->bridge_pass == 3 & posMat[(alien->row)][(alien->column)+1] == 0) 
				{
					alien->x += alien->moveSpeed;
					alien->cont += alien->moveSpeed;
					alien->mapPos = 1;
					
					if(alien->cont >= 20) 
					{
						posMat[(alien->row)][(alien->column)] = 0;
						//printf("contador_pixeles: %f\n",alien->cont);
						alien->column += 1;
						posMat[alien->row][alien->column] = alien->alienID;
						alien->cont = 0;
						alien->mapPos = 0;
					}
				}
				if(alien->x >= 920 & alien->y >= 500 & alien->bridge_pass == 3 & posMat[(alien->row)-1][(alien->column)] == 0) 
				{
					alien->y -= alien->moveSpeed;
					if(alien->y <= 500) alien->bridge_pass = 4;
					alien->cont += alien->moveSpeed;
					alien->mapPos = 1;
					
					if(alien->cont >= 20) 
					{
						posMat[(alien->row)][(alien->column)] = 0;
						//printf("contador_pixeles: %f\n",alien->cont);
						alien->row -= 1;
						posMat[alien->row][alien->column] = alien->alienID;
						alien->cont = 0;
						alien->mapPos = 0;
					}
				}
			}
		}

		*alienResult = *alien;
		return alienResult;
	}
	else
	{
		if(alien->y <= 560 & alien->x == 840 & alien->bridge_pass == 0 & posMat[(alien->row)+1][(alien->column)] == 0)
		{
			alien->y += alien->moveSpeed;
			alien->cont += alien->moveSpeed;
			alien->mapPos = 1;
			
			if(alien->cont >= 20) 
			{
				posMat[(alien->row)][(alien->column)] = 0;
				//printf("contador_pixeles: %f\n",alien->cont);
				alien->row += 1;
				posMat[alien->row][alien->column] = alien->alienID;
				alien->cont = 0;
				alien->mapPos = 0;
			}
		}
		if(alien->y >= 560 & alien->x >= 600 & alien->bridge_pass == 0 & posMat[(alien->row)][(alien->column)-1] == 0)
		{
			alien->x -= alien->moveSpeed;
			alien->cont += alien->moveSpeed;
			alien->mapPos = 1;
			
			if(alien->cont >= 20) 
			{
				posMat[(alien->row)][(alien->column)] = 0;
				//printf("contador_pixeles: %f\n",alien->cont);
				alien->column -= 1;
				posMat[alien->row][alien->column] = alien->alienID;
				alien->cont = 0;
				alien->mapPos = 0;
			}
		}
		if(alien->y >= 500 & alien->x <= 600 & alien->bridge_pass == 0 & posMat[(alien->row)-1][(alien->column)] == 0)
		{
			alien->y -= alien->moveSpeed;
			alien->cont += alien->moveSpeed;
			alien->mapPos = 1;
			
			if(alien->cont >= 20) 
			{
				posMat[(alien->row)][(alien->column)] = 0;
				//printf("contador_pixeles: %f\n",alien->cont);
				alien->row -= 1;
				posMat[alien->row][alien->column] = alien->alienID;
				alien->cont = 0;
				alien->mapPos = 0;
			}
		}
		if(alien->y <= 500)
		{
			if(alien->bridge_ID == 'c')
			{
				if(alien->x <= 600 & alien->y >= 460 & alien->bridge_pass == 0 & posMat[(alien->row)-1][(alien->column)] == 0)
				{
					alien->y -= alien->moveSpeed;
					alien->cont += alien->moveSpeed;
					alien->mapPos = 1;
					
					if(alien->cont >= 20) 
					{
						posMat[(alien->row)][(alien->column)] = 0;
						//printf("contador_pixeles: %f\n",alien->cont);
						alien->row -= 1;
						posMat[alien->row][alien->column] = alien->alienID;
						alien->cont = 0;
						alien->mapPos = 0;
					}
				}
				if(alien->x >= 500 & alien->y <= 460 & alien->bridge_pass == 0 & posMat[(alien->row)][(alien->column)-1] == 0)
				{
					alien->x -= alien->moveSpeed;
					alien->cont += alien->moveSpeed;
					alien->mapPos = 1;
					
					if(alien->cont >= 20) 
					{
						posMat[(alien->row)][(alien->column)] = 0;
						//printf("contador_pixeles: %f\n",alien->cont);
						alien->column -= 1;
						posMat[alien->row][alien->column] = alien->alienID;
						alien->cont = 0;
						alien->mapPos = 0;
					}
				}
				if(alien->x <= 500 & alien->y >= 400 & alien->bridge_pass == 0 & posMat[(alien->row)-1][(alien->column)] == 0) 
				{
					alien->y -= alien->moveSpeed;
					if(alien->y <= 402) alien->bridge_pass = 2;
					alien->cont += alien->moveSpeed;
					alien->mapPos = 1;
					
					if(alien->cont >= 20) 
					{
						posMat[(alien->row)][(alien->column)] = 0;
						//printf("contador_pixeles: %f\n",alien->cont);
						alien->row -= 1;
						posMat[alien->row][alien->column] = alien->alienID;
						alien->cont = 0;
						alien->mapPos = 0;
					}
				}
				if(alien->x >= 475 & alien->y >= 200 & (alien->bridge_pass == 2 | alien->bridge_pass == 6)) // Waiting for algorithms
				{
					if(alien->y >= 380){
						if(bridge2_algorithm == 3){
							if(posMat[(alien->row)-1][(alien->column)] == 0 & posMat[(alien->row)-1][(alien->column)-1] == 0 & posMat[(alien->row)-2][(alien->column)-1] == 0 & posMat[(alien->row)-3][(alien->column)-1] == 0 & posMat[(alien->row)-4][(alien->column)-1] == 0 & posMat[(alien->row)-5][(alien->column)-1] == 0 & posMat[(alien->row)-6][(alien->column)-1] == 0){
								alien->y -= alien->moveSpeed;
								alien->cont += alien->moveSpeed;
								alien->mapPos = 1;
								
								if(alien->cont >= 20) 
								{
									posMat[(alien->row)][(alien->column)] = 0;
									//printf("contador_pixeles: %f\n",alien->cont);
									alien->row -= 1;
									posMat[alien->row][alien->column] = alien->alienID;
									alien->cont = 0;
									alien->mapPos = 0;
								}

							}

						}
						else{
							if(aliensFlags[(alien->alienID)-1]){
							
								posMat[(alien->row)][(alien->column)] = 0;
								alien->x = 500;
								alien->y = 380;
								alien->row = 19;
								alien->column = 25;
								aliensFlags[(alien->alienID)-1] = 2;
							}
							//printf("flag en c: %d\n",aliensFlags[(alien->alienID)-1]);
							if(aliensFlags[(alien->alienID)-1] == 2){
							
								if(posMat[(alien->row)-1][(alien->column)] == 0) // Waiting for algorithms
								{
									alien->y -= alien->moveSpeed;
									alien->cont += alien->moveSpeed;
									alien->mapPos = 1;
									internal_flag = 0;
									
									if(alien->cont >= 20) 
									{
										posMat[(alien->row)][(alien->column)] = 0;
										//printf("contador_pixeles: %f\n",alien->cont);
										alien->row -= 1;
										posMat[alien->row][alien->column] = alien->alienID;
										alien->cont = 0;
										alien->mapPos = 0;
									}
								}
								
								
							}
						}
					
					} 
					if(alien->y <= 380 & alien->x >= 480 & alien->bridge_pass != 6 & posMat[(alien->row)][(alien->column)-1] == 0){
						alien->x -= alien->moveSpeed;
						alien->cont += alien->moveSpeed;
						alien->mapPos = 1;
						
						if(alien->cont >= 20) 
						{
							posMat[(alien->row)][(alien->column)] = 0;
							//printf("contador_pixeles: %f\n",alien->cont);
							alien->column -= 1;
							posMat[alien->row][alien->column] = alien->alienID;
							alien->cont = 0;
							alien->mapPos = 0;
						}
					} 
					if(alien->y >= 280 & alien->x <= 480 /* & posMat[(alien->row)-1][(alien->column)] == 0 */) 
					{
						if(bridge2_algorithm != 3){
							if(alien->y >= 280){
								alien->y -= alien->moveSpeed;
								if(alien->y <= 283) alien->bridge_pass = 6;
								alien->cont += alien->moveSpeed;
								alien->mapPos = 1;
								
								if(alien->cont >= 20) 
								{
									posMat[(alien->row)][(alien->column)] = 0;
									//printf("contador_pixeles: %f\n",alien->cont);
									alien->row -= 1;
									posMat[alien->row][alien->column] = alien->alienID;
									alien->cont = 0;
									alien->mapPos = 0;
								}

							}
							else{
								if(posMat[(alien->row)-1][(alien->column)] == 0){
								alien->y -= alien->moveSpeed;
								if(alien->y <= 283) alien->bridge_pass = 6;
								alien->cont += alien->moveSpeed;
								alien->mapPos = 1;
								
								if(alien->cont >= 20) 
								{
									posMat[(alien->row)][(alien->column)] = 0;
									//printf("contador_pixeles: %f\n",alien->cont);
									alien->row -= 1;
									posMat[alien->row][alien->column] = alien->alienID;
									alien->cont = 0;
									alien->mapPos = 0;
								}
							}
							}
							
						}
						else{
							alien->y -= alien->moveSpeed;
							if(alien->y <= 283) alien->bridge_pass = 6;
							alien->cont += alien->moveSpeed;
							alien->mapPos = 1;
							
							if(alien->cont >= 20) 
							{
								posMat[(alien->row)][(alien->column)] = 0;
								//printf("contador_pixeles: %f\n",alien->cont);
								alien->row -= 1;
								posMat[alien->row][alien->column] = alien->alienID;
								alien->cont = 0;
								alien->mapPos = 0;
							}
						}
						
					}
					if(alien->y <= 280 & alien->x <= 500 & posMat[(alien->row)][(alien->column)+1] == 0){
						alien->x += alien->moveSpeed;
						alien->cont += alien->moveSpeed;
						alien->mapPos = 1;
						
						if(alien->cont >= 20) 
						{
							posMat[(alien->row)][(alien->column)] = 0;
							//printf("contador_pixeles: %f\n",alien->cont);
							alien->column += 1;
							posMat[alien->row][alien->column] = alien->alienID;
							alien->cont = 0;
							alien->mapPos = 0;
						}
					} 
					if(alien->y >= 200 & alien->x >= 500 & posMat[(alien->row)-1][(alien->column)] == 0){
						alien->y -= alien->moveSpeed;
						alien->cont += alien->moveSpeed;
						alien->mapPos = 1;
						
						if(alien->cont >= 20) 
						{
							posMat[(alien->row)][(alien->column)] = 0;
							//printf("contador_pixeles: %f\n",alien->cont);
							alien->row -= 1;
							posMat[alien->row][alien->column] = alien->alienID;
							alien->cont = 0;
							alien->mapPos = 0;
						}
					} 
					if(alien->y <= 200) alien->bridge_pass = 1;
				}
				if(alien->x <= 540 & alien->y <= 200 & alien->bridge_pass == 1 & posMat[(alien->row)][(alien->column)+1] == 0) 
				{
					alien->x += alien->moveSpeed;
					alien->cont += alien->moveSpeed;
					alien->mapPos = 1;
					
					if(alien->cont >= 20) 
					{
						posMat[(alien->row)][(alien->column)] = 0;
						//printf("contador_pixeles: %f\n",alien->cont);
						alien->column += 1;
						posMat[alien->row][alien->column] = alien->alienID;
						alien->cont = 0;
						alien->mapPos = 0;
					}
				}
				if(alien->x >= 540 & alien->y >= 20 & alien->bridge_pass == 1 & posMat[(alien->row)-1][(alien->column)] == 0) 
				{
					alien->y -= alien->moveSpeed;
					if(alien->y <= 20) alien->bridge_pass = 3;
					alien->cont += alien->moveSpeed;
					alien->mapPos = 1;
					
					if(alien->cont >= 20) 
					{
						posMat[(alien->row)][(alien->column)] = 0;
						//printf("contador_pixeles: %f\n",alien->cont);
						alien->row -= 1;
						posMat[alien->row][alien->column] = alien->alienID;
						alien->cont = 0;
						alien->mapPos = 0;
					}
				}
				if(alien->x >= 60 & alien->y <= 20 & alien->bridge_pass == 3 & posMat[(alien->row)][(alien->column)-1] == 0) 
				{
					alien->x -= alien->moveSpeed;
					alien->cont += alien->moveSpeed;
					alien->mapPos = 1;
					
					if(alien->cont >= 20) 
					{
						posMat[(alien->row)][(alien->column)] = 0;
						//printf("contador_pixeles: %f\n",alien->cont);
						alien->column -= 1;
						posMat[alien->row][alien->column] = alien->alienID;
						alien->cont = 0;
						alien->mapPos = 0;
					}
				}
				if(alien->x <= 60 & alien->y <= 160 & alien->bridge_pass == 3 & posMat[(alien->row)+1][(alien->column)] == 0) 
				{
					alien->y += alien->moveSpeed;
					if(alien->y >= 160) alien->bridge_pass = 4;
					alien->cont += alien->moveSpeed;
					alien->mapPos = 1;
					
					if(alien->cont >= 20) 
					{
						posMat[(alien->row)][(alien->column)] = 0;
						//printf("contador_pixeles: %f\n",alien->cont);
						alien->row += 1;
						posMat[alien->row][alien->column] = alien->alienID;
						alien->cont = 0;
						alien->mapPos = 0;
					}
				}
				
			}
			if(alien->bridge_ID == 'w')
			{
				if(alien->x <= 600 & alien->y >= 480 & alien->bridge_pass == 0 & posMat[(alien->row)-1][(alien->column)] == 0)
				{
					alien->y -= alien->moveSpeed;
					alien->cont += alien->moveSpeed;
					alien->mapPos = 1;
					
					if(alien->cont >= 20) 
					{
						posMat[(alien->row)][(alien->column)] = 0;
						//printf("contador_pixeles: %f\n",alien->cont);
						alien->row -= 1;
						posMat[alien->row][alien->column] = alien->alienID;
						alien->cont = 0;
						alien->mapPos = 0;
					}
				}
				if(alien->x <= 660 & alien->y <= 480 & alien->bridge_pass == 0 & posMat[(alien->row)][(alien->column)+1] == 0)
				{
					alien->x += alien->moveSpeed;
					alien->cont += alien->moveSpeed;
					alien->mapPos = 1;
					
					if(alien->cont >= 20) 
					{
						posMat[(alien->row)][(alien->column)] = 0;
						//printf("contador_pixeles: %f\n",alien->cont);
						alien->column += 1;
						posMat[alien->row][alien->column] = alien->alienID;
						alien->cont = 0;
						alien->mapPos = 0;
					}
				}
				if(alien->x >= 660 & alien->y >= 400 & alien->bridge_pass == 0 & posMat[(alien->row)-1][(alien->column)] == 0) 
				{
					alien->y -= alien->moveSpeed;
					if(alien->y <= 400) alien->bridge_pass = 2;
					alien->cont += alien->moveSpeed;
					alien->mapPos = 1; 
					
					if(alien->cont >= 20) 
					{
						posMat[(alien->row)][(alien->column)] = 0;
						//printf("contador_pixeles: %f\n",alien->cont);
						alien->row -= 1;
						posMat[alien->row][alien->column] = alien->alienID;
						alien->cont = 0;
						alien->mapPos = 0;
					}
				}
				if(alien->x >= 635 & alien->y >= 180 & (alien->bridge_pass == 2 | alien->bridge_pass == 6)) // Waiting for algorithms
				{
					if(alien->y >= 380){
						if(bridge3_algorithm == 3){
							if(posMat[(alien->row)-1][(alien->column)] == 0 & posMat[(alien->row)-1][(alien->column)-1] == 0 & posMat[(alien->row)-2][(alien->column)-1] == 0 & posMat[(alien->row)-3][(alien->column)-1] == 0 & posMat[(alien->row)-4][(alien->column)-1] == 0 & posMat[(alien->row)-5][(alien->column)-1] == 0 & posMat[(alien->row)-6][(alien->column)-1] == 0){
								alien->y -= alien->moveSpeed;
								alien->cont += alien->moveSpeed;
								alien->mapPos = 1;
								
								if(alien->cont >= 20) 
								{
									posMat[(alien->row)][(alien->column)] = 0;
									//printf("contador_pixeles: %f\n",alien->cont);
									alien->row -= 1;
									posMat[alien->row][alien->column] = alien->alienID;
									alien->cont = 0;
									alien->mapPos = 0;
								}

							}

						}
						else{
							if(aliensFlags[(alien->alienID)-1]){
				
								posMat[(alien->row)][(alien->column)] = 0;
								alien->x = 660;
								alien->y = 380;
								alien->row = 19;
								alien->column = 33;
								aliensFlags[(alien->alienID)-1] = 2;
							}
						
							if(aliensFlags[(alien->alienID)-1] == 2){
							
								if(posMat[(alien->row)-1][(alien->column)] == 0) // Waiting for algorithms
								{
									alien->y -= alien->moveSpeed;
									alien->cont += alien->moveSpeed;
									alien->mapPos = 1;
									internal_flag = 0;
									
									if(alien->cont >= 20) 
									{
										posMat[(alien->row)][(alien->column)] = 0;
										//printf("contador_pixeles: %f\n",alien->cont);
										alien->row -= 1;
										posMat[alien->row][alien->column] = alien->alienID;
										alien->cont = 0;
										alien->mapPos = 0;
									}
									
								}
								
								
							}
						}

					} 
					if(alien->y <= 380 & alien->x >= 640 & alien->bridge_pass != 6 & posMat[(alien->row)][(alien->column)-1] == 0){
						alien->x -= alien->moveSpeed;
						alien->cont += alien->moveSpeed;
						alien->mapPos = 1;
						
						if(alien->cont >= 20) 
						{
							posMat[(alien->row)][(alien->column)] = 0;
							//printf("contador_pixeles: %f\n",alien->cont);
							alien->column -= 1;
							posMat[alien->row][alien->column] = alien->alienID;
							alien->cont = 0;
							alien->mapPos = 0;
						}
					} 
					if(alien->y >= 280 & alien->x <= 640 /* & posMat[(alien->row)-1][(alien->column)] == 0 */) 
					{
						if(bridge3_algorithm != 3){
							if(alien->y >= 280){
								alien->y -= alien->moveSpeed;
								if(alien->y <= 283) alien->bridge_pass = 6;
								alien->cont += alien->moveSpeed;
								alien->mapPos = 1;
								
								if(alien->cont >= 20) 
								{
									posMat[(alien->row)][(alien->column)] = 0;
									//printf("contador_pixeles: %f\n",alien->cont);
									alien->row -= 1;
									posMat[alien->row][alien->column] = alien->alienID;
									alien->cont = 0;
									alien->mapPos = 0;
								}
							}
							else{
								if(posMat[(alien->row)-1][(alien->column)] == 0){
									alien->y -= alien->moveSpeed;
									if(alien->y <= 283) alien->bridge_pass = 6;
									alien->cont += alien->moveSpeed;
									alien->mapPos = 1;
									
									if(alien->cont >= 20) 
									{
										posMat[(alien->row)][(alien->column)] = 0;
										//printf("contador_pixeles: %f\n",alien->cont);
										alien->row -= 1;
										posMat[alien->row][alien->column] = alien->alienID;
										alien->cont = 0;
										alien->mapPos = 0;
									}

								}
							}
							
						}
						else{
							alien->y -= alien->moveSpeed;
							if(alien->y <= 283) alien->bridge_pass = 6;
							alien->cont += alien->moveSpeed;
							alien->mapPos = 1;
							
							if(alien->cont >= 20) 
							{
								posMat[(alien->row)][(alien->column)] = 0;
								//printf("contador_pixeles: %f\n",alien->cont);
								alien->row -= 1;
								posMat[alien->row][alien->column] = alien->alienID;
								alien->cont = 0;
								alien->mapPos = 0;
							}
						}
						
					}
					if(alien->y <= 280 & alien->x <= 660 & posMat[(alien->row)][(alien->column)+1] == 0){
						alien->x += alien->moveSpeed;
						alien->cont += alien->moveSpeed;
						alien->mapPos = 1;
						
						if(alien->cont >= 20) 
						{
							posMat[(alien->row)][(alien->column)] = 0;
							//printf("contador_pixeles: %f\n",alien->cont);
							alien->column += 1;
							posMat[alien->row][alien->column] = alien->alienID;
							alien->cont = 0;
							alien->mapPos = 0;
						}
					} 
					if(alien->y >= 180 & alien->x >= 660 & alien->y <= 280 & posMat[(alien->row)-1][(alien->column)] == 0){
						alien->y -= alien->moveSpeed;
						alien->cont += alien->moveSpeed;
						alien->mapPos = 1;
						
						if(alien->cont >= 20) 
						{
							posMat[(alien->row)][(alien->column)] = 0;
							//printf("contador_pixeles: %f\n",alien->cont);
							alien->row -= 1;
							posMat[alien->row][alien->column] = alien->alienID;
							alien->cont = 0;
							alien->mapPos = 0;
						}
					}
					 
					if(alien->y <= 180) alien->bridge_pass = 1;
				}
				if(alien->x >= 540 & alien->y <= 180 & alien->bridge_pass == 1 & posMat[(alien->row)][(alien->column)-1] == 0) 
				{
					alien->x -= alien->moveSpeed;
					alien->cont += alien->moveSpeed;
					alien->mapPos = 1;
					
					if(alien->cont >= 20) 
					{
						posMat[(alien->row)][(alien->column)] = 0;
						//printf("contador_pixeles: %f\n",alien->cont);
						alien->column -= 1;
						posMat[alien->row][alien->column] = alien->alienID;
						alien->cont = 0;
						alien->mapPos = 0;
					}
				}
				if(alien->x <= 540 & alien->y >= 20 & alien->bridge_pass == 1 & posMat[(alien->row)-1][(alien->column)] == 0) 
				{
					alien->y -= alien->moveSpeed;
					if(alien->y <= 20) alien->bridge_pass = 3;
					alien->cont += alien->moveSpeed;
					alien->mapPos = 1;
					
					if(alien->cont >= 20) 
					{
						posMat[(alien->row)][(alien->column)] = 0;
						//printf("contador_pixeles: %f\n",alien->cont);
						alien->row -= 1;
						posMat[alien->row][alien->column] = alien->alienID;
						alien->cont = 0;
						alien->mapPos = 0;
					}
				}
				if(alien->x >= 60 & alien->y <= 20 & alien->bridge_pass == 3 & posMat[(alien->row)][(alien->column)-1] == 0) 
				{
					alien->x -= alien->moveSpeed;
					alien->cont += alien->moveSpeed;
					alien->mapPos = 1;
					
					if(alien->cont >= 20) 
					{
						posMat[(alien->row)][(alien->column)] = 0;
						//printf("contador_pixeles: %f\n",alien->cont);
						alien->column -= 1;
						posMat[alien->row][alien->column] = alien->alienID;
						alien->cont = 0;
						alien->mapPos = 0;
					}
				}
				if(alien->x <= 60 & alien->y <= 160 & alien->bridge_pass == 3 & posMat[(alien->row)+1][(alien->column)] == 0) 
				{
					alien->y += alien->moveSpeed;
					if(alien->y >= 160) alien->bridge_pass = 4;
					alien->cont += alien->moveSpeed;
					alien->mapPos = 1;
					
					if(alien->cont >= 20) 
					{
						posMat[(alien->row)][(alien->column)] = 0;
						//printf("contador_pixeles: %f\n",alien->cont);
						alien->row += 1;
						posMat[alien->row][alien->column] = alien->alienID;
						alien->cont = 0;
						alien->mapPos = 0;
					}
				}
			}
			if(alien->bridge_ID == 'e')
			{
				if(alien->x <= 600 & alien->y >= 460 & alien->bridge_pass == 0 & posMat[(alien->row)-1][(alien->column)] == 0)
				{
					alien->y -= alien->moveSpeed;
					if(alien->y <= 20) alien->bridge_pass = 3;
					alien->cont += alien->moveSpeed;
					alien->mapPos = 1;
					
					if(alien->cont >= 20) 
					{
						posMat[(alien->row)][(alien->column)] = 0;
						//printf("contador_pixeles: %f\n",alien->cont);
						alien->row -= 1;
						posMat[alien->row][alien->column] = alien->alienID;
						alien->cont = 0;
						alien->mapPos = 0;
					}
				}
				if(alien->x >= 340 & alien->y <= 460 & alien->bridge_pass == 0 & posMat[(alien->row)][(alien->column)-1] == 0)
				{
					alien->x -= alien->moveSpeed;
					alien->cont += alien->moveSpeed;
					alien->mapPos = 1;
					
					if(alien->cont >= 20) 
					{
						posMat[(alien->row)][(alien->column)] = 0;
						//printf("contador_pixeles: %f\n",alien->cont);
						alien->column -= 1;
						posMat[alien->row][alien->column] = alien->alienID;
						alien->cont = 0;
						alien->mapPos = 0;
					}
				}
				if(alien->x <= 340 & alien->y >= 400 & alien->bridge_pass == 0 & posMat[(alien->row)-1][(alien->column)] == 0) 
				{
					alien->y -= alien->moveSpeed;
					if(alien->y <= 402) alien->bridge_pass = 2;
					alien->cont += alien->moveSpeed;
					alien->mapPos = 1;
					
					if(alien->cont >= 20) 
					{
						posMat[(alien->row)][(alien->column)] = 0;
						//printf("contador_pixeles: %f\n",alien->cont);
						alien->row -= 1;
						posMat[alien->row][alien->column] = alien->alienID;
						alien->cont = 0;
						alien->mapPos = 0;
					}
				}
				if(alien->x >= 315 & alien->y >= 200 & (alien->bridge_pass == 2 | alien->bridge_pass == 6)) // Waiting for algorithms
				{
					if(alien->y >= 380){
						if(bridge1_algorithm == 3){
							if(posMat[(alien->row)-1][(alien->column)] == 0 & posMat[(alien->row)-1][(alien->column)-1] == 0 & posMat[(alien->row)-2][(alien->column)-1] == 0 & posMat[(alien->row)-3][(alien->column)-1] == 0 & posMat[(alien->row)-4][(alien->column)-1] == 0 & posMat[(alien->row)-5][(alien->column)-1] == 0 & posMat[(alien->row)-6][(alien->column)-1] == 0){
								alien->y -= alien->moveSpeed;
								alien->cont += alien->moveSpeed;
								alien->mapPos = 1;
								
								if(alien->cont >= 20) 
								{
									posMat[(alien->row)][(alien->column)] = 0;
									//printf("contador_pixeles: %f\n",alien->cont);
									alien->row -= 1;
									posMat[alien->row][alien->column] = alien->alienID;
									alien->cont = 0;
									alien->mapPos = 0;
								}

							}

						}
						else{
							if(aliensFlags[(alien->alienID)-1]){
								
								posMat[(alien->row)][(alien->column)] = 0;
								alien->x = 340;
								alien->y = 380;
								alien->row = 19;
								alien->column = 17;
								aliensFlags[(alien->alienID)-1] = 2;
							}
							else if(aliensFlags[(alien->alienID)-1] == 2){
								if(posMat[(alien->row)-1][(alien->column)] == 0) // Waiting for algorithms
								{
									alien->y -= alien->moveSpeed;
									alien->cont += alien->moveSpeed;
									alien->mapPos = 1;
									
									if(alien->cont >= 20) 
									{
										posMat[(alien->row)][(alien->column)] = 0;
										//printf("contador_pixeles: %f\n",alien->cont);
										alien->row -= 1;
										posMat[alien->row][alien->column] = alien->alienID;
										alien->cont = 0;
										alien->mapPos = 0;
									}
									
								}
								
								
							}
						}
					} 
					if(alien->y <= 380 & alien->x >= 320 & alien->bridge_pass != 6 & posMat[(alien->row)][(alien->column)-1] == 0){
						alien->x -= alien->moveSpeed;
						alien->cont += alien->moveSpeed;
						alien->mapPos = 1;
						
						if(alien->cont >= 20) 
						{
							posMat[(alien->row)][(alien->column)] = 0;
							//printf("contador_pixeles: %f\n",alien->cont);
							alien->column -= 1;
							posMat[alien->row][alien->column] = alien->alienID;
							alien->cont = 0;
							alien->mapPos = 0;
						}
					} 
					if(alien->y >= 280 & alien->x <= 320 /* & posMat[(alien->row)-1][(alien->column)] == 0 */) 
					{
						if(bridge1_algorithm != 3){
							if(alien->y >= 280){
								alien->y -= alien->moveSpeed;
								if(alien->y <= 283) alien->bridge_pass = 6;
								alien->cont += alien->moveSpeed;
								alien->mapPos = 1;
								
								if(alien->cont >= 20) 
								{
									posMat[(alien->row)][(alien->column)] = 0;
									//printf("contador_pixeles: %f\n",alien->cont);
									alien->row -= 1;
									posMat[alien->row][alien->column] = alien->alienID;
									alien->cont = 0;
									alien->mapPos = 0;
								}
							}
							else{
								if(posMat[(alien->row)-1][(alien->column)] == 0){
									alien->y -= alien->moveSpeed;
									if(alien->y <= 283) alien->bridge_pass = 6;
									alien->cont += alien->moveSpeed;
									alien->mapPos = 1;
									
									if(alien->cont >= 20) 
									{
										posMat[(alien->row)][(alien->column)] = 0;
										//printf("contador_pixeles: %f\n",alien->cont);
										alien->row -= 1;
										posMat[alien->row][alien->column] = alien->alienID;
										alien->cont = 0;
										alien->mapPos = 0;
									}
								
								}

							}
							
						}
						else{
							alien->y -= alien->moveSpeed;
							if(alien->y <= 283) alien->bridge_pass = 6;
							alien->cont += alien->moveSpeed;
							alien->mapPos = 1;
							
							if(alien->cont >= 20) 
							{
								posMat[(alien->row)][(alien->column)] = 0;
								//printf("contador_pixeles: %f\n",alien->cont);
								alien->row -= 1;
								posMat[alien->row][alien->column] = alien->alienID;
								alien->cont = 0;
								alien->mapPos = 0;
							}
						}
						
					}
					if(alien->y <= 280 & alien->x <= 340 & posMat[(alien->row)][(alien->column)+1] == 0){
						alien->x += alien->moveSpeed;
						alien->cont += alien->moveSpeed;
						alien->mapPos = 1;
						
						if(alien->cont >= 20) 
						{
							posMat[(alien->row)][(alien->column)] = 0;
							//printf("contador_pixeles: %f\n",alien->cont);
							alien->column += 1;
							posMat[alien->row][alien->column] = alien->alienID;
							alien->cont = 0;
							alien->mapPos = 0;
						}
					} 
					if(alien->y >= 200 & alien->x >= 340 & posMat[(alien->row)-1][(alien->column)] == 0){
						alien->y -= alien->moveSpeed;
						alien->cont += alien->moveSpeed;
						alien->mapPos = 1;
						
						if(alien->cont >= 20) 
						{
							posMat[(alien->row)][(alien->column)] = 0;
							//printf("contador_pixeles: %f\n",alien->cont);
							alien->row -= 1;
							posMat[alien->row][alien->column] = alien->alienID;
							alien->cont = 0;
							alien->mapPos = 0;
						}
					} 
					if(alien->y <= 200) alien->bridge_pass = 1;
				}
				if(alien->x <= 540 & alien->y <= 200 & alien->bridge_pass == 1 & posMat[(alien->row)][(alien->column)+1] == 0) 
				{
					alien->x += alien->moveSpeed;
					alien->cont += alien->moveSpeed;
					alien->mapPos = 1;
					
					if(alien->cont >= 20) 
					{
						posMat[(alien->row)][(alien->column)] = 0;
						//printf("contador_pixeles: %f\n",alien->cont);
						alien->column += 1;
						posMat[alien->row][alien->column] = alien->alienID;
						alien->cont = 0;
						alien->mapPos = 0;
					}
				}
				if(alien->x >= 540 & alien->y >= 20 & alien->bridge_pass == 1 & posMat[(alien->row)-1][(alien->column)] == 0) 
				{
					alien->y -= alien->moveSpeed;
					if(alien->y <= 20) alien->bridge_pass = 3;
					alien->cont += alien->moveSpeed;
					alien->mapPos = 1;
					
					if(alien->cont >= 20) 
					{
						posMat[(alien->row)][(alien->column)] = 0;
						//printf("contador_pixeles: %f\n",alien->cont);
						alien->row -= 1;
						posMat[alien->row][alien->column] = alien->alienID;
						alien->cont = 0;
						alien->mapPos = 0;
					}
				}
				if(alien->x >= 60 & alien->y <= 20 & alien->bridge_pass == 3 & posMat[(alien->row)][(alien->column)-1] == 0) 
				{
					alien->x -= alien->moveSpeed;
					alien->cont += alien->moveSpeed;
					alien->mapPos = 1;
					
					if(alien->cont >= 20) 
					{
						posMat[(alien->row)][(alien->column)] = 0;
						//printf("contador_pixeles: %f\n",alien->cont);
						alien->column -= 1;
						posMat[alien->row][alien->column] = alien->alienID;
						alien->cont = 0;
						alien->mapPos = 0;
					}
				}
				if(alien->x <= 60 & alien->y <= 160 & alien->bridge_pass == 3 & posMat[(alien->row)+1][(alien->column)] == 0) 
				{
					alien->y += alien->moveSpeed;
					if(alien->y >= 160) alien->bridge_pass = 4;
					alien->cont += alien->moveSpeed;
					alien->mapPos = 1;
					
					if(alien->cont >= 20) 
					{
						posMat[(alien->row)][(alien->column)] = 0;
						//printf("contador_pixeles: %f\n",alien->cont);
						alien->row += 1;
						posMat[alien->row][alien->column] = alien->alienID;
						alien->cont = 0;
						alien->mapPos = 0;
					}
				}
			}
		}

		*alienResult = *alien;
		pthread_exit(alienResult);
		return alienResult;
	}		
}

void drawMap(int map[][size])
{
	int jp = 0;
	for(int i = 0; i < mapSizeX; i++)
	{
		for(int j = 0; j < mapSizeY; j++)
		{
			if(map[i][j] == 1)
			{
				al_draw_filled_rectangle(i*blockSize, j*blockSize, 
				i*blockSize + blockSize, j*blockSize + blockSize, al_map_rgb(100,0,200));
			}
			if(map[i][j] == 2)
			{
				al_draw_filled_rectangle(i*blockSize, j*blockSize, 
				i*blockSize + blockSize, j*blockSize + blockSize, al_map_rgb(250,0,214));
			}
			if(map[i][j] == 3)
			{
				
				al_draw_filled_rectangle(i*blockSize, j*blockSize, 
				i*blockSize + blockSize, j*blockSize + blockSize, al_map_rgb(153,100,214));
			}
			if(map[i][j] == 4)
			{
				al_draw_filled_rectangle(i*blockSize, j*blockSize, 
				i*blockSize + blockSize, j*blockSize + blockSize, al_map_rgb(159,150,214));
			}
			if(map[i][j] == 5)
			{
				al_draw_filled_rectangle(i*blockSize, j*blockSize, 
				i*blockSize + blockSize, j*blockSize + blockSize, al_map_rgb(255,0,0));
			}
			
		}
	}
}


void drawAliens()
{
	// <--------------------------------------- Bridge Zones ---------------------------------------->
	// Center Northbridge
	al_draw_line(540, 190, 490, 190, al_map_rgba( 0, 0, 0, 128), 4); // Signal
	al_draw_line(490, 190, 490, 210, al_map_rgba( 0, 0, 0, 128), 4); // Signal
	al_draw_filled_triangle(495, 210, 485, 210, 490, 220, al_map_rgb(0,0,0));
	/* al_draw_bitmap(alienN, 480, 260, 0);
	al_draw_bitmap(alienN, 500, 260, 0); */

	// Center Southbridge
	al_draw_line(470, 490, 510, 490, al_map_rgba( 0, 0, 0, 128), 4); // Signal
	al_draw_line(510, 490, 510, 470, al_map_rgba( 0, 0, 0, 128), 4); // Signal
	al_draw_filled_triangle(515, 470, 505, 470, 510, 460, al_map_rgb(0,0,0));
	/* al_draw_bitmap(alienA, 480, 400, 0);
	al_draw_bitmap(alienA, 500, 400, 0);

	// East Northbridge
	al_draw_bitmap(alienA, 320, 260, 0);
	al_draw_bitmap(alienN, 340, 260, 0);

	// East Southbridge
	al_draw_bitmap(alienB, 320, 400, 0);
	al_draw_bitmap(alienB, 340, 400, 0);

	// West Northbridge
	al_draw_bitmap(alienA, 640, 260, 0);
	al_draw_bitmap(alienB, 660, 260, 0);

	// West Southbridge
	al_draw_bitmap(alienB, 640, 400, 0);
	al_draw_bitmap(alienN, 660, 400, 0); */

	// <--------------------------------------- Core Zones ---------------------------------------->
	// Core corner Up-left (outer)
	al_draw_line(360, 190, 330, 190, al_map_rgba( 0, 0, 0, 128), 4); // Signal
	al_draw_line(330, 190, 330, 210, al_map_rgba( 0, 0, 0, 128), 4); // Signal
	al_draw_filled_triangle(325, 210, 335, 210, 330, 220, al_map_rgb(0,0,0));
	/* al_draw_bitmap(alienI, 320, 180, 0); */

	// Core corner Up-right (outer)
	al_draw_line(670, 220, 670, 190, al_map_rgba( 0, 0, 0, 128), 4); // Signal
	al_draw_line(670, 190, 650, 190, al_map_rgba( 0, 0, 0, 128), 4); // Signal
	al_draw_filled_triangle(650, 185, 650, 195, 640, 190, al_map_rgb(0,0,0));
	/* al_draw_bitmap(alienI, 660, 180, 0); */

	// Core corner Down-left (outer)
	al_draw_line(330, 460, 330, 490, al_map_rgba( 0, 0, 0, 128), 4); // Signal
	al_draw_line(330, 490, 350, 490, al_map_rgba( 0, 0, 0, 128), 4); // Signal
	al_draw_filled_triangle(350, 495, 350, 485, 360, 490, al_map_rgb(0,0,0));
	/* al_draw_bitmap(alienI, 320, 480, 0); */

	// Core corner Down-right (outer)
	al_draw_line(640, 490, 670, 490, al_map_rgba( 0, 0, 0, 128), 4); // Signal
	al_draw_line(670, 490, 670, 470, al_map_rgba( 0, 0, 0, 128), 4); // Signal
	al_draw_filled_triangle(675, 470, 665, 470, 670, 460, al_map_rgb(0,0,0));
	/* al_draw_bitmap(alienI, 660, 480, 0); */

	// Core corner Up-left (inner)
	al_draw_line(350, 210, 350, 240, al_map_rgba( 0, 0, 0, 128), 4); // Signal
	al_draw_line(350, 210, 370, 210, al_map_rgba( 0, 0, 0, 128), 4); // Signal
	al_draw_filled_triangle(370, 215, 370, 205, 380, 210, al_map_rgb(0,0,0));
	/* al_draw_bitmap(alienA, 340, 200, 0); */

	// Core corner Up-right (inner)
	al_draw_line(620, 210, 650, 210, al_map_rgba( 0, 0, 0, 128), 4); // Signal
	al_draw_line(650, 210, 650, 230, al_map_rgba( 0, 0, 0, 128), 4); // Signal
	al_draw_filled_triangle(655, 230, 645, 230, 650, 240, al_map_rgb(0,0,0));
	/* al_draw_bitmap(alienA, 640, 200, 0); */

	// Core corner Down-left (inner)
	al_draw_line(380, 470, 350, 470, al_map_rgba( 0, 0, 0, 128), 4); // Signal
	al_draw_line(350, 470, 350, 450, al_map_rgba( 0, 0, 0, 128), 4); // Signal
	al_draw_filled_triangle(355, 450, 345, 450, 350, 440, al_map_rgb(0,0,0));
	/* al_draw_bitmap(alienA, 340, 460, 0); */

	// Core corner Down-right (inner)
	al_draw_line(650, 440, 650, 470, al_map_rgba( 0, 0, 0, 128), 4); // Signal
	al_draw_line(650, 470, 630, 470, al_map_rgba( 0, 0, 0, 128), 4); // Signal
	al_draw_filled_triangle(630, 475, 630, 465, 620, 470, al_map_rgb(0,0,0));
	/* al_draw_bitmap(alienA, 640, 460, 0); */

	// <--------------------------------------- BaseA Zones ---------------------------------------->

	// BaseA Entry
	/* al_draw_bitmap(alienN, 60, 160, 0); */
	al_draw_line(240, 30, 300, 30, al_map_rgba( 0, 0, 0, 128), 4); // Signal
	al_draw_filled_triangle(240, 25, 240, 35, 230, 30, al_map_rgb(0,0,0));

	// BaseA Exit
	/* al_draw_bitmap(alienA, 140, 160, 0); */
	al_draw_line(240, 90, 300, 90, al_map_rgba( 0, 0, 0, 128), 4); // Signal
	al_draw_filled_triangle(300, 85, 300, 95, 310, 90, al_map_rgb(0,0,0));

	/* // BaseA Entry corner1
	al_draw_bitmap(alienN, 60, 20, 0);

	// BaseA Entry corner2
	al_draw_bitmap(alienA, 540, 20, 0);

	// BaseA Exit corner1
	al_draw_bitmap(alienB, 140, 80, 0);

	// BaseA Exit corner2
	al_draw_bitmap(alienN, 380, 80, 0);

	// BaseA Entry Crossing
	al_draw_bitmap(alienA, 540, 160, 0);

	// BaseA Exit Crossing
	al_draw_bitmap(alienB, 380, 160, 0); */

	// <--------------------------------------- BaseB Zones ---------------------------------------->

	// BaseB Exit 
	/* al_draw_bitmap(alienB, 840, 500, 0); */
	al_draw_line(640, 570, 700, 570, al_map_rgba( 0, 0, 0, 128), 4); // Signal
	al_draw_filled_triangle(640, 575, 640, 565, 630, 570, al_map_rgb(0,0,0));

	// BaseB Entry
	/* al_draw_bitmap(alienN, 920, 500, 0); */
	al_draw_line(640, 630, 700, 630, al_map_rgba( 0, 0, 0, 128), 4); // Signal
	al_draw_filled_triangle(700, 635, 700, 625, 710, 630, al_map_rgb(0,0,0));

	/* // BaseB Entry corner1
	al_draw_bitmap(alienA, 920, 620, 0);

	// BaseB Entry corner2
	al_draw_bitmap(alienB, 440, 620, 0);

	// BaseB Exit corner1
	al_draw_bitmap(alienN, 840, 560, 0);

	// BaseB Exit corner2
	al_draw_bitmap(alienA, 600, 560, 0);

	// BaseB Entry Crossing
	al_draw_bitmap(alienN, 440, 500, 0);

	// BaseB Exit Crossing
	al_draw_bitmap(alienA, 600, 500, 0); */
}


void LoadMap(const char *filename, int map[][size])
{
	FILE *fp1;
    fp1 = fopen(filename, "r");

    if(NULL == fp1)
    {
        printf("Error opening file");
        exit(1);
    }

    char line[size];

    while(fgets(line, 256, fp1) != NULL)
    {
		int len = strlen(line);
		
		for(int i = 0; i <= len; i++)
		{
			if(line[i] == '0')
			{
				map[loadCounterX][loadCounterY] = 0;
				loadCounterX++;
			}
			if(line[i] == '1')
			{
				map[loadCounterX][loadCounterY] = 1;
				loadCounterX++;
			}
			if(line[i] == '2')
			{
				map[loadCounterX][loadCounterY] = 2;
				loadCounterX++;
			}

			if(line[i] == '3')
			{
				map[loadCounterX][loadCounterY] = 3;
				loadCounterX++;
			}
			if(line[i] == '4')
			{
				map[loadCounterX][loadCounterY] = 4;
				loadCounterX++;
			}
			if(line[i] == '5')
			{
				map[loadCounterX][loadCounterY] = 5;
				loadCounterX++;
			}
		}

		loadCounterY++;
		loadCounterX = 0;      
    }

	fclose(fp1); 
}

char* itoa(int val, int base){
	
	static char buf[32] = {0};
	char * zero = "0";
	
	int i = 30;

	if(val != 0)
	{
		for(; val && i ; --i, val /= base)
		{
			buf[i] = "0123456789abcdef"[val % base];
    	}
	}
	else
	{
		return zero;
	}

	return &buf[i+1];	
}

struct Alien autoGenerateAliens(int speed_base, int beta_max_time, int normal_weight, int alfa_weight,int beta_weight, int count_id, int code, int type){
	int type_alien;
	if(type == 3) type_alien = randint(3); //0=normal, 1=alfa; 2=beta;
	else type_alien = type;
	int bridge = randint (3); //0=e, 1=c; 2=w;
	int community;
	if(code == 2) community = randint(2); //0 = a, 1= b
	else community = code;
	if(bridge == 0){
		if(community == 0){
			if( type_alien == 0){
				struct Alien generated = {140,160,speed_base,normal_weight,community,type_alien,0,alienN,'e',0,8,7,0.0,count_id,0,1,0};
				return generated;

			}
			if( type_alien == 1){
				struct Alien generated = {140,160,speed_base,alfa_weight,community,type_alien,0,alienA,'e',0,8,7,0.0,count_id,0,2,0};
				return generated;
			}
			if( type_alien == 2){
				struct Alien generated = {140,160,(((speed_base)*1)+speed_base),beta_weight,community,type_alien,0,alienB,'e',0,8,7,0.0,count_id,0,3,beta_max_time};
				return generated;
			}
		}
		else{
			if( type_alien == 0){
				struct Alien generated = {840,500,speed_base,normal_weight,community,type_alien,0,alienN,'e',0,25,42,0.0,count_id,0,1,0};
				return generated;
			}
			if( type_alien == 1){
				struct Alien generated = {840,500,speed_base,alfa_weight,community,type_alien,0,alienA,'e',0,25,42,0.0,count_id,0,2,0};
				return generated;
			
			}
			if( type_alien == 2){
				struct Alien generated = {840,500,(((speed_base)*1)+speed_base),beta_weight,community,type_alien,0,alienB,'e',0,25,42,0.0,count_id,0,3,beta_max_time};
				return generated;
			}
		}
	}
	else if(bridge == 1){
		if(community == 0){
			if( type_alien == 0){
				struct Alien generated = {140,160,speed_base,normal_weight,community,type_alien,0,alienN,'c',0,8,7,0.0,count_id,0,1,0};
				return generated;

			}
			if( type_alien == 1){
				struct Alien generated = {140,160,speed_base,alfa_weight,community,type_alien,0,alienA,'c',0,8,7,0.0,count_id,0,2,0};
				return generated;
			}
			if( type_alien == 2){
				struct Alien generated = {140,160,(((speed_base)*1)+speed_base),beta_weight,community,type_alien,0,alienB,'c',0,8,7,0.0,count_id,0,3,beta_max_time};
				return generated;
			}
		}
		else{
			if( type_alien == 0){
				struct Alien generated = {840,500,speed_base,normal_weight,community,type_alien,0,alienN,'c',0,25,42,0.0,count_id,0,1,0};
				return generated;
			}
			if( type_alien == 1){
				struct Alien generated = {840,500,speed_base,alfa_weight,community,type_alien,0,alienA,'c',0,25,42,0.0,count_id,0,2,0};
				return generated;
			
			}
			if( type_alien == 2){
				struct Alien generated = {840,500,(((speed_base)*1)+speed_base),beta_weight,community,type_alien,0,alienB,'c',0,25,42,0.0,count_id,0,3,beta_weight};
				return generated;
			}
		}
	}
	else if(bridge == 2){
		if(community == 0){
			if( type_alien == 0){
				struct Alien generated = {140,160,speed_base,normal_weight,community,type_alien,0,alienN,'w',0,8,7,0.0,count_id,0,1,0};
				return generated;

			}
			if( type_alien == 1){
				struct Alien generated = {140,160,speed_base,alfa_weight,community,type_alien,0,alienA,'w',0,8,7,0.0,count_id,0,2,0};
				return generated;
			}
			if( type_alien == 2){
				struct Alien generated = {140,160,(((speed_base)*1)+speed_base),beta_weight,community,type_alien,0,alienB,'w',0,8,7,0.0,count_id,0,3,beta_max_time};
				return generated;
			}
		}
		else{
			if( type_alien == 0){
				struct Alien generated = {840,500,speed_base,normal_weight,community,type_alien,0,alienN,'w',0,25,42,0.0,count_id,0,1,0};
				return generated;
			}
			if( type_alien == 1){
				struct Alien generated = {840,500,speed_base,alfa_weight,community,type_alien,0,alienA,'w',0,25,42,0.0,count_id,0,2,0};
				return generated;
			
			}
			if( type_alien == 2){
				struct Alien generated = {840,500,(((speed_base)*1)+speed_base),beta_weight,community,type_alien,0,alienB,'w',0,25,42,0.0,count_id,0,3,beta_max_time};
				return generated;
			}
		}
	}
}

int load_speed_base(){
	FILE *fp1;
    fp1 = fopen(ALIEN_NORMAL_CONFIG, "r"); 
    if(NULL == fp1)
    {
        printf("Error opening file");
        return 1;
    }
    char line[256];
    int linenum=0;
    while(fgets(line, 256, fp1) != NULL)
    {
        char LINE[256], speed[256];
        linenum++;
        if(sscanf(line, "%s %s", LINE, speed) != 2)
        {
            fprintf(stderr, "Syntax error, line %d\n", linenum);
            continue;
        }
        if(linenum==1){
            //printf("Line %d:  IP %s MAC %s\n", linenum, LINE, speed);
            return atoi(speed);
        }      
    }  
}

int load_normal_weight(){
	FILE *fp1;
    fp1 = fopen(ALIEN_NORMAL_CONFIG, "r"); 
    if(NULL == fp1)
    {
        printf("Error opening file");
        return 1;
    }
    char line[256];
    int linenum=0;
    while(fgets(line, 256, fp1) != NULL)
    {
        char LINE[256], weight[256];
        linenum++;
        if(sscanf(line, "%s %s", LINE, weight) != 2)
        {
            fprintf(stderr, "Syntax error, line %d\n", linenum);
            continue;
        }
        if(linenum==2){
            //printf("Line %d:  IP %s MAC %s\n", linenum, LINE, weight);
            return atoi(weight);
        }      
    }  
}

int load_alfa_weight(){
	FILE *fp1;
    fp1 = fopen(ALIEN_ALFA_CONFIG, "r"); 
    if(NULL == fp1)
    {
        printf("Error opening file");
        return 1;
    }
    char line[256];
    int linenum=0;
    while(fgets(line, 256, fp1) != NULL)
    {
        char LINE[256], weight[256];
        linenum++;
        if(sscanf(line, "%s %s", LINE, weight) != 2)
        {
            fprintf(stderr, "Syntax error, line %d\n", linenum);
            continue;
        }
        if(linenum==1){
            //printf("Line %d:  IP %s MAC %s\n", linenum, LINE, weight);
            return atoi(weight);
        }      
    }  
}

int load_beta_max_time(){
	FILE *fp1;
    fp1 = fopen(ALIEN_BETA_CONFIG, "r"); 
    if(NULL == fp1)
    {
        printf("Error opening file");
        return 1;
    }
    char line[256];
    int linenum=0;
    while(fgets(line, 256, fp1) != NULL)
    {
        char LINE[256], max_time[256];
        linenum++;
        if(sscanf(line, "%s %s", LINE, max_time) != 2)
        {
            fprintf(stderr, "Syntax error, line %d\n", linenum);
            continue;
        }
        if(linenum==1){
            //printf("Line %d:  IP %s MAC %s\n", linenum, LINE, max_time);
            return atoi(max_time);
        }      
    }  
}

int load_beta_weight(){
	FILE *fp1;
    fp1 = fopen(ALIEN_BETA_CONFIG, "r"); 
    if(NULL == fp1)
    {
        printf("Error opening file");
        return 1;
    }
    char line[256];
    int linenum=0;
    while(fgets(line, 256, fp1) != NULL)
    {
        char LINE[256], weight[256];
        linenum++;
        if(sscanf(line, "%s %s", LINE, weight) != 2)
        {
            fprintf(stderr, "Syntax error, line %d\n", linenum);
            continue;
        }
        if(linenum==2){
            //printf("Line %d:  IP %s MAC %s\n", linenum, LINE, weight);
            return atoi(weight);
        }      
    }  
}

int load_bridge1_algorithm(){
	FILE *fp1;
    fp1 = fopen(BRIDGE1_CONFIG, "r"); 
    if(NULL == fp1)
    {
        printf("Error opening file");
        return 1;
    }
    char line[256];
    int linenum=0;
    while(fgets(line, 256, fp1) != NULL)
    {
        char LINE[256], algorithm[256];
        linenum++;
        if(sscanf(line, "%s %s", LINE, algorithm) != 2)
        {
            fprintf(stderr, "Syntax error, line %d\n", linenum);
            continue;
        }
        if(linenum==1){
            //printf("Line %d:  IP %s MAC %s\n", linenum, LINE, algorithm);
            return atoi(algorithm);
        }      
    }  
}
int load_bridge2_algorithm(){
	FILE *fp1;
    fp1 = fopen(BRIDGE2_CONFIG, "r"); 
    if(NULL == fp1)
    {
        printf("Error opening file");
        return 1;
    }
    char line[256];
    int linenum=0;
    while(fgets(line, 256, fp1) != NULL)
    {
        char LINE[256], algorithm[256];
        linenum++;
        if(sscanf(line, "%s %s", LINE, algorithm) != 2)
        {
            fprintf(stderr, "Syntax error, line %d\n", linenum);
            continue;
        }
        if(linenum==1){
            //printf("Line %d:  IP %s MAC %s\n", linenum, LINE, algorithm);
            return atoi(algorithm);
        }      
    }  
}
int load_bridge3_algorithm(){
	FILE *fp1;
    fp1 = fopen(BRIDGE3_CONFIG, "r"); 
    if(NULL == fp1)
    {
        printf("Error opening file");
        return 1;
    }
    char line[256];
    int linenum=0;
    while(fgets(line, 256, fp1) != NULL)
    {
        char LINE[256], algorithm[256];
        linenum++;
        if(sscanf(line, "%s %s", LINE, algorithm) != 2)
        {
            fprintf(stderr, "Syntax error, line %d\n", linenum);
            continue;
        }
        if(linenum==1){
            //printf("Line %d:  IP %s MAC %s\n", linenum, LINE, algorithm);
            return atoi(algorithm);
        }      
    }  
}
int load_bridge1_candelarizador(){
	FILE *fp1;
    fp1 = fopen(BRIDGE1_CONFIG, "r"); 
    if(NULL == fp1)
    {
        printf("Error opening file");
        return 1;
    }
    char line[256];
    int linenum=0;
    while(fgets(line, 256, fp1) != NULL)
    {
        char LINE[256], candelarizador[256];
        linenum++;
        if(sscanf(line, "%s %s", LINE, candelarizador) != 2)
        {
            fprintf(stderr, "Syntax error, line %d\n", linenum);
            continue;
        }
        if(linenum==2){
            //printf("Line %d:  IP %s MAC %s\n", linenum, LINE, candelarizador);
            return atoi(candelarizador);
        }      
    }  
}
int load_bridge2_candelarizador(){
	FILE *fp1;
    fp1 = fopen(BRIDGE2_CONFIG, "r"); 
    if(NULL == fp1)
    {
        printf("Error opening file");
        return 1;
    }
    char line[256];
    int linenum=0;
    while(fgets(line, 256, fp1) != NULL)
    {
        char LINE[256], candelarizador[256];
        linenum++;
        if(sscanf(line, "%s %s", LINE, candelarizador) != 2)
        {
            fprintf(stderr, "Syntax error, line %d\n", linenum);
            continue;
        }
        if(linenum==2){
            //printf("Line %d:  IP %s MAC %s\n", linenum, LINE, candelarizador);
            return atoi(candelarizador);
        }      
    }  
}
int load_bridge3_candelarizador(){
	FILE *fp1;
    fp1 = fopen(BRIDGE3_CONFIG, "r"); 
    if(NULL == fp1)
    {
        printf("Error opening file");
        return 1;
    }
    char line[256];
    int linenum=0;
    while(fgets(line, 256, fp1) != NULL)
    {
        char LINE[256], candelarizador[256];
        linenum++;
        if(sscanf(line, "%s %s", LINE, candelarizador) != 2)
        {
            fprintf(stderr, "Syntax error, line %d\n", linenum);
            continue;
        }
        if(linenum==2){
            //printf("Line %d:  IP %s MAC %s\n", linenum, LINE, candelarizador);
            return atoi(candelarizador);
        }      
    }  
}
int load_bridge1_weight(){
	FILE *fp1;
    fp1 = fopen(BRIDGE1_CONFIG, "r"); 
    if(NULL == fp1)
    {
        printf("Error opening file");
        return 1;
    }
    char line[256];
    int linenum=0;
    while(fgets(line, 256, fp1) != NULL)
    {
        char LINE[256], weight[256];
        linenum++;
        if(sscanf(line, "%s %s", LINE, weight) != 2)
        {
            fprintf(stderr, "Syntax error, line %d\n", linenum);
            continue;
        }
        if(linenum==3){
            //printf("Line %d:  IP %s MAC %s\n", linenum, LINE, weight);
            return atoi(weight);
        }      
    }  
}
int load_bridge2_weight(){
	FILE *fp1;
    fp1 = fopen(BRIDGE2_CONFIG, "r"); 
    if(NULL == fp1)
    {
        printf("Error opening file");
        return 1;
    }
    char line[256];
    int linenum=0;
    while(fgets(line, 256, fp1) != NULL)
    {
        char LINE[256], weight[256];
        linenum++;
        if(sscanf(line, "%s %s", LINE, weight) != 2)
        {
            fprintf(stderr, "Syntax error, line %d\n", linenum);
            continue;
        }
        if(linenum==3){
            //printf("Line %d:  IP %s MAC %s\n", linenum, LINE, weight);
            return atoi(weight);
        }      
    }  
}
int load_bridge3_weight(){
	FILE *fp1;
    fp1 = fopen(BRIDGE3_CONFIG, "r"); 
    if(NULL == fp1)
    {
        printf("Error opening file");
        return 1;
    }
    char line[256];
    int linenum=0;
    while(fgets(line, 256, fp1) != NULL)
    {
        char LINE[256], weight[256];
        linenum++;
        if(sscanf(line, "%s %s", LINE, weight) != 2)
        {
            fprintf(stderr, "Syntax error, line %d\n", linenum);
            continue;
        }
        if(linenum==3){
            //printf("Line %d:  IP %s MAC %s\n", linenum, LINE, weight);
            return atoi(weight);
        }      
    }  
}

int alien_random(){
    return (int) randint(2) % 3;
}

int bridge_random(){
    return (int) randint(2) % 3;
}

int community_random(){
    return (int) randint(2) % 2;
}

int randint(int n) {
  if ((n - 1) == RAND_MAX) {
    return rand();
  } else {
    // Supporting larger values for n would requires an even more
    // elaborate implementation that combines multiple calls to rand()
    assert (n <= RAND_MAX);

    // Chop off all of the values that would cause skew...
    int end = RAND_MAX / n; // truncate skew
    assert (end > 0);
    end *= n;

    // ... and ignore results from rand() that fall above that limit.
    // (Worst case the loop condition should succeed 50% of the time,
    // so we can expect to bail out of this loop pretty quickly.)
    int r;
    while ((r = rand()) >= end);

    return r % n;
  }
}

void * AlgorithmsThread(void *arg){
    struct parametros * data1;
    struct parametros data;
    data1=(struct parametros *)arg;
    data=*data1;
    int size=data.size;
    char bridge=data.bridge;
    int cant=data.cant; 
    int timeN=data.timeN; 
    int timeS=data.timeS;
    int algorithm=data.algorithm; 
    int BrigdeWeight=data.BrigdeWeight; 
    int scheduler=data.scheduler; 

    mainAlgorithm(size, bridge, cant, timeN, timeS, algorithm, BrigdeWeight, scheduler, *aliens,*aliensM,&mode,posMat,aliensFlags,go); //color nombre correcto a esas últimas
}