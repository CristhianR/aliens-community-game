#include <stdio.h>
#include <stdlib.h>
#include <math.h>
#include <time.h>

double U_Random();

double possion(int Lambda) /* generates a random number with a Poisson distribution. Lamda is the average number */
{
	double k = 0.0;
	long double p = 1.0;
	long double l=exp(-Lambda); /* it is defined as long double for precision, and exp (-Lambda) is a decimal near 0 */
	while (p>=l)
	{
		double u = U_Random();
		p *= u;
		k++;
	}
	return k-1;
}

double U_Random() /* generates a 0 ~ Random number between 1 */
{
	static int done = 0;
	int number;
	if(!done)  
	{  
		srand((int)time(0));
		done = 1;
	}
	number=1+(int)(100.0*rand()/(RAND_MAX+1.0));
	return number/100.0;
}