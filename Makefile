CC = gcc
CFLAGS = -g -c
AR = ar -rc
RANLIB = ranlib


Target: my_pthread.a

my_pthread.a: my_pthread.o
	$(AR) libmy_pthread.a my_pthread.o
	$(RANLIB) libmy_pthread.a

my_pthread.o: my_pthread_t.h
	$(CC) -pthread $(CFLAGS) my_pthread.c

clean:
	rm -rf testfile *.o *.a *.bin

runlib: 
	$(CC) -I. -g  -c main.c -o main.o 
	$(CC) -o main -I. -g main.o -lallegro -lallegro_font -lallegro_primitives -lallegro_image libmy_pthread.a

run:
	$(CC) main.c -o main -lallegro -lallegro_font -lallegro_primitives -lallegro_image -lallegro_ttf -pthread -lrt -lm
	./main
